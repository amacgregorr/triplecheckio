<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::get('/', 'LandingController@index');

Route::get('profile', function () {
    return 'Welcome. Your Email address is ' . Auth::user()->email . 'and the ID: ' . Auth::user()->id;
})->before('auth');

Route::group(array('before'=>'auth'), function() {
    Route::resource('dashboard', 'DashboardController', array('only' => array('index')));
    Route::post('projects/{id}', array('as' => 'project.update', 'uses' => 'ProjectsController@update'));
    Route::get('dashboard/project/{id}', 'ProjectsController@view');
    Route::get('dashboard/project/edit-build/id/{id}', 'ProjectsController@editBuild');
    Route::get('dashboard/project/edit-settings/id/{id}', 'ProjectsController@editSettings');
    Route::post('dashboard/project/build/', 'BuildController@store');
});

//Route::get('/report', 'ConnectController@report');
Route::get('/', 'ConnectController@report');
Route::get('/list', 'ConnectController@index');
Route::get('/details/{id}', 'ConnectController@view');
Route::post('/search', 'ConnectController@search');

Route::get('project/hook/{token}/', 'WebhookController@trigger');
Route::post('project/hook/{token}/', 'WebhookController@trigger');

Route::get('/badge/{type}/{id}', 'BadgeController@show');


Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController', array('only' => array('create','store','destroy')));