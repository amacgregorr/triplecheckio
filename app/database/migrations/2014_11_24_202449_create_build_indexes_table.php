<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBuildIndexesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('build_indexes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('build_id');
            $table->string('name');
            $table->string('author');
            $table->float('score');
            $table->string('grade')->default('');
            $table->text('description');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('build_indexes');
	}

}
