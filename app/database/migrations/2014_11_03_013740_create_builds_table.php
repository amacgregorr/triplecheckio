<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBuildsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('builds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('project_id')->default(0);
			$table->text('configuration');
			$table->text('score_data');
			$table->text('result');
            $table->boolean('is_public')->default(false);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('builds');
	}

}
