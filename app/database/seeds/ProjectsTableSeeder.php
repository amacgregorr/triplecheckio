<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProjectsTableSeeder extends Seeder {

	public function run()
	{
        $defaultTaskConfig = '{"tasks":[{"type":"phpcs","settings":{"ignore_list":[{"type":"file","path":"app\/Mage.php"},{"type":"file","path":"api.php"},{"type":"file","path":"get.php"},{"type":"file","path":"install.php"},{"type":"file","path":"cron.php"},{"type":"directory","path":"js/"},{"type":"file","path":"index.php"},{"type":"directory","path":"app/code/core/"},{"type":"directory","path":"spec"},{"type":"directory","path":"features/"},{"type":"directory","path":"downloader/"},{"type":"directory","path":"lib/"},{"type":"directory","path":"shell/"},{"type":"directory","path":"errors/"},{"type":"directory","path":"skin/"},{"type":"directory","path":"app\/design\/adminhtml/"},{"type":"directory","path":"app\/design\/frontend\/base\/"},{"type":"directory","path":"app\/design\/frontend\/enterprise\/"}],"file_extensions":["php","phtml"],"standards":["ECG"],"report":"json","files":[]}}]}';

        //{"type": "svn", "url": "https://demacmedia.svn.beanstalkapp.com/demac-extensions/Chase/development", "username": "amacgregor", "password": "Macgregor6969"}
        $projectList = array(
            array(
                'name'        => 'Ardene',
                'url'         => 'http://ardene.com',
                'description' => 'Ardene.com is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/ardn-ptnr01/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Boathouse',
                'url'         => 'http://boathousestores.com',
                'description' => 'Boathouse is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/bhou-ptnr01/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Cosmo Music',
                'url'         => 'http://cosmomusic.ca',
                'description' => 'Cosmomusic.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/cosm-ptnr01/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'ECS Coffee',
                'url'         => 'http://ecscoffee.ca',
                'description' => 'ECS Coffee.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/ecsc-ptnr/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'eLUXE',
                'url'         => 'http://eluxe.ca',
                'description' => 'eLUXE.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/elux-ptnr01/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Bench.ca',
                'url'         => 'http://bench.ca',
                'description' => 'Bench.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/fmrk-ptnr/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'GRAFIC',
                'url'         => 'http://grafic.ca',
                'description' => 'GRAFIC.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/grfc-ptnr01/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Perma Brands',
                'url'         => 'http://permabrands.ca',
                'description' => 'Perma Brands.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/perm-ptnr/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Pink Tartan',
                'url'         => 'http://pinktartan.ca',
                'description' => 'Pink Tartan.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/pktn-ptnr01/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Rens Pets Depot',
                'url'         => 'http://renspets.ca',
                'description' => 'Rens Pets.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/rens-ptnr01/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Snugglebugz',
                'url'         => 'http://snugglebugz.ca',
                'description' => 'Snugglebugz is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/sbug-ptnr0/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Stokes',
                'url'         => 'http://stokestores.ca',
                'description' => 'StokeStores.ca is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/stks-csvc/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            ),
            array(
                'name'        => 'Alberta',
                'url'         => 'http://goav.ca',
                'description' => 'Alberta. is a Magento powered store.',
                'task_config' => $defaultTaskConfig,
                'vcs'         => json_encode(array("type" => "svn", "url" => "https://demacmedia.svn.beanstalkapp.com/umbr-csvc/trunk/", "username" => "amacgregor", "password" => "Macgregor6969"))
            )
        );


        foreach ($projectList as $project) {
            Project::create($project);
        }
	}

}