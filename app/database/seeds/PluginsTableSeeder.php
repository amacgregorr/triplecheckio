<?php

class PluginsTableSeeder extends Seeder {

	public function run()
	{
		Plugin::create(array(
			'name'              => 'PHP CodeSniffer',
			'code'              => 'phpcs',
			'class_name'        => "Plugin_Phpcs",
			'description'       => 'PHP_CodeSniffer is a PHP5 script that tokenises PHP, JavaScript and CSS files to detect violations of a defined coding standard. It is an essential development tool that ensures your code remains clean and consistent. It can also help prevent some common semantic errors made by developers.',
			'config_template'   => '{ "settings": { "ignore_list": [ { "type": "directory", "path": "app/code/core/"} ], "file_extensions": ["php", "phtml"], "standards": ["ECG","Zend"], "report": "json", "files": [ "app/code/local/Demac/Bogo/Model/Observer.php", "app/code/local/Demac/"] } }'
		));

        Plugin::create(array(
            'name'              => 'Magento Rewrite Check',
            'code'              => 'magentoRewrite',
            'class_name'        => "Plugin_MagentoRewrite",
            'description'       => 'Magento Rewrite.',
            'config_template'   => '{ "settings": {} }'
        ));

	}

}