<?php
error_reporting(0);

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class BuildRunner extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'crimson:build-runner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return \BuildRunner
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     *
     * @return mixed
     */
    public function fire()
    {
        $scoreOnly = $this->option('recalculate-scores');
        if($scoreOnly) {
            // Load the Build Queue
            $Queue = Build::all();
            foreach ($Queue as $build) {

                $scoreBuilder = new Scoring_Builder;
                try {
                    $buildResults = json_decode($build->result,true);
                    $buildScores  = $scoreBuilder->run($buildResults);

                    $build->score_data = json_encode($buildScores);

                    $build->save();

                    echo ".";
                } catch (Exception $e) {
                    $exception = "Build {$build->id} - {$e->getMessage()}";
                    Log::error($exception);
                    echo "E";
                }
            }
            return;
        }


        // Load the Build Queue
        $Queue = BuildQueue::where('status', '=', 'pending')->get();

        // Iterate through the build requests
        foreach ($Queue as $buildRequest) {

            $scoreBuilder = new Scoring_Builder;

            try {
                $buildRequest->markAs('processing');
                $buildData = json_decode($buildRequest->configuration);

                list($storagePath, $repoPath) = $this->preparePaths($buildData, $buildRequest);

                $this->validateStoratePath($storagePath);
                $this->checkoutRepository($buildData, $repoPath);

                $buildResults = $this->runBuildTasks($buildData, $repoPath);
                $buildScores  = $scoreBuilder->run($buildResults);

                $this->createBuild($buildRequest, $buildData, $buildResults, $buildScores);

                $buildRequest->markAs('completed');
                echo ".";
            } catch (Exception $e) {
                $exception = "BuildRequest {$buildRequest->id} - {$e->getMessage()}";
                Log::error($exception);

                $buildRequest->status = 'failed';
                $buildRequest->save();
                echo "E";
            }
        }
    }

    // Start [Refactor] : The whole Scoring logic should be on a separate class
    protected function calculateScore($results)
    {
        $scores           = array();
        $calculatedScores = array();

        $errors   = 0;
        $warnings = 0;
        $healthy  = 0;

        $errors_files   = 0;
        $warnings_files = 0;
        $healthy_files  = 0;

        // Start [Refactor] : This needs to be changed foreach inside a foreach are ugly as hell
        foreach ($results as $plugin => $pluginData) {
            if ($plugin == 'magerewrite') {
                continue;
            }
            foreach ($pluginData['files'] as $file) {
                if ($file['scores']['errors'] > 0) {
                    $errors += $file['scores']['errors'];
                    $errors_files++;
                } elseif ($file['scores']['warnings'] > 0) {
                    $warnings += $file['scores']['warnings'];
                    $warnings_files++;
                } else {
                    $healthy++;
                    $healthy_files++;
                }

                $scores[] = $file['scores']['avg_score'];
            }
        }
        // End Refactor

        if (count($scores) > 0) {
            $calculatedScores['avg_score'] = array_sum($scores) / count($scores);
        } else {
            $calculatedScores['avg_score'] = 0;
        }

        $calculatedScores['breakdown'] = array(
            array('type' => 'errors', 'score' => $errors, 'file_count' => $errors_files),
            array('type' => 'healthy', 'score' => $healthy, 'file_count' => $healthy_files),
            array('type' => 'warnings', 'score' => $warnings, 'file_count' => $warnings_files),
        );

        return json_encode($calculatedScores);
    }
    // End Refactor

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//			array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('recalculate-scores', null, InputOption::VALUE_OPTIONAL, 'Recalculate scores for already completed builds.', null),
        );
    }

    /**
     * @param $buildData
     * @param $buildRequest
     * @param $storagePath
     * @return string
     */
    protected function _getFullRepoPath($buildData, $buildRequest, $storagePath)
    {
        $repoDirName = str_replace(" ", "_", strtolower("{$buildData->configuration->project->name}_{$buildData->configuration->vcs->type}_{$buildRequest->project->id}"));
        $repoPath    = $storagePath . $repoDirName;
        return $repoPath;
    }

    /**
     * @param $buildData
     * @param $buildRequest
     * @return array
     */
    protected function preparePaths($buildData, $buildRequest)
    {
        $storagePath = storage_path() . '/builds/repositories/';
        $repoPath    = $this->_getFullRepoPath($buildData, $buildRequest, $storagePath);
        return array($storagePath, $repoPath);
    }

    /**
     * @param $storagePath
     */
    protected function validateStoratePath($storagePath)
    {
        if (!File::exists($storagePath)) {
            File::makeDirectory($storagePath, $mode = 0777, $recursive = true);
        }
    }

    /**
     * @param $buildData
     * @param $repoPath
     */
    protected function checkoutRepository($buildData, $repoPath)
    {
        // Set Repo path
        $buildData->configuration->vcs->destination = $repoPath;

        $vcsAdapter = VcsBuilder::create($buildData->configuration->vcs->type);
        $vcsAdapter->setConfiguration($buildData->configuration->vcs);
        $vcsAdapter->validateConfiguration();
        $vcsAdapter->validatePath($repoPath);
        $vcsAdapter->run();
    }

    /**
     * @param $plugin
     * @param $task
     */
    protected function configurePlugin($plugin, $task)
    {
        if ($plugin instanceof Configurable_Interface) {
            $plugin->setConfiguration($task->settings);
        }
    }

    /**
     * @param $buildData
     * @param $repoPath
     * @return array
     */
    protected function runBuildTasks($buildData, $repoPath)
    {
        $buildResults = array();

        foreach ($buildData->tasks as $task) {
            $plugin = Plugin::where('code', $task->type)->first();
            $this->configurePlugin($plugin, $task);
            $plugin->setPath($repoPath);
            $plugin->run($repoPath);
            $buildResults = array_merge_recursive($buildResults, $plugin->formatResult());

        }
        return $buildResults;
    }

    /**
     * @param $buildRequest
     * @param $buildData
     * @param $buildResults
     */
    protected function createBuild($buildRequest, $buildData, $buildResults, $buildScores)
    {
        $build                = new Build;
        $build->project_id    = $buildRequest->project_id;
        $build->configuration = json_encode($buildData);
        $build->result        = json_encode($buildResults);
        $build->score_data    = json_encode($buildScores);

        if ($buildRequest->project->user_id == 0) {
            $build->is_public = true;
        }

        $build->save();
    }
}
