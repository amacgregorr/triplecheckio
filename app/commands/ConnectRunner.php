<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class ConnectRunner
 *
 * This is in charge with downloading, parsing and scheduling the extensions from MagentoConnect for analysis.
 */
class ConnectRunner extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'mage:connect-runner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download the magento connect extensions and schedule them for a build';

    /**
     * @var
     */
    protected $command;

    /**
     * Create a new command instance.
     *
     * @return \ConnectRunner
     */
    public function __construct()
    {
        parent::__construct();

        $this->command = L4shell::get();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // Get the list from connect
        $matches = $this->getExtensionList();
        $matches = array_reverse($matches, true);

        foreach ($matches as $id => $extensionData)
        {
            try {

                $extensionName 	= $extensionData['name'];
				$currentVersion = $extensionData['version'];

                $project        = $this->getProjectInstance($extensionName);
                $storagePath    = $this->getRepositoryStoragePath($extensionName, $project->id);
                $extensionFile  = $this->getExtensionFilePath($extensionName, $currentVersion);

                $this->checkIfRepositoryDirectoryExists($storagePath);

                // Check if the current version is the one analyzed

                // Start [Refactor] : Handle existing projects and requeue them
                if (File::exists($extensionFile)) {
                    echo 'S';
                    continue;
                }
                // End Refactor

                $result = $this->command->setCommand('cd tools/magento/ && ./mage download community %s')
                    ->setExecutablePath()
                    ->setArguments(array(
                        $extensionName
                    ))
                    ->setAllowedCharacters(array("&"))
                    ->execute();

                $result = str_replace("Saved to: ", "", $result);

                $this->command->setCommand('tar -xzvf %s -C %s')
                    ->setExecutablePath()
                    ->setArguments(array(
                        $result,
                        $storagePath
                    ))
                    ->setAllowedCharacters(array("&"))
                    ->execute();

                $packageInfo = $this->parseConnectPackageInformation($storagePath);

                $vcsArray = array(
                    "type"      => "connect",
                    "version"   => $extensionData['version'],
                    "path"      => $storagePath,
                    "stability" => $extensionData['stability']
                );

                $tasks = $this->getTaskTemplate();

                $this->updateProject($extensionName, $project, $vcsArray, $packageInfo, $tasks);

                // Prepare the project configuration data
                $buildConfiguration = $project->getBuildConfigurationData();

                $this->createBuildRequest($project, $buildConfiguration);


                echo '.';
            } catch (Exception $e) {
				echo 'E';
				$exception = "There was a problem processing {$extensionName} \r\n " .
					         "Failed with Exception \r\n {$e->getMessage()} \r\n" .
							 "{$e->getTraceAsString()} \r\n";

				Log::error($exception);

            }
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//			array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('rerun-existing', null, InputOption::VALUE_NONE, 'If a extension already exists requeue it.', null)
        );
    }

    /**
     * Check if the Repository Directory exists and if it doesn't create it
     *
     * @param $storagePath
     * @test testCheckIfRepositoryDirectoryExists
     */
    protected function checkIfRepositoryDirectoryExists($storagePath)
    {
        if (!File::exists($storagePath)) {
            File::makeDirectory($storagePath, $mode = 0777, $recursive = true);
        }
    }

    /**
     * Get The extension list from Magento connect
     *
     * @return mixed
     */
    protected function getExtensionList()
    {
        $result = $this->command->setCommand('cd tools/magento/ && ./mage list-available')
            ->setExecutablePath()
            ->setAllowedCharacters(array("&"))
            ->execute();

        $matches = $this->parseMagentoConnectList($result);

        return $matches;
    }

    /**
     * Will return a new project blank instance of the project or return the existing one
     *
     * @param $extensionName
     * @return Project
     */
    protected function getProjectInstance($extensionName)
    {
        $project = Project::where('name', $extensionName)->first();

        if (!is_null($project)) {
            return $project;
        }

        $project = new Project;

        $project->name        = $extensionName;
        $project->vcs         = '';
        $project->description = '';
        $project->user_id     = 0;
        $project->task_config = '';
        $project->url         = '';

        $project->save();
        return $project;
    }

    /**
     * Generate the repository path based on the extension name and project_id
     *
     * @param $extensionName
     * @param $project_id
     * @return string
     */
    protected function getRepositoryStoragePath($extensionName, $project_id)
    {
        $repoDirName = str_replace(" ", "_", strtolower("{$extensionName}_connect_{$project_id}"));
        $storagePath = storage_path() . '/builds/repositories/' . $repoDirName;
        return $storagePath;
    }

    /**
     * Load the task configuration from the task template file
     *
     * @return string
     */
    protected function getTaskTemplate()
    {
        $taskTemplate = storage_path() . '/builds/templates/connect/tasks.json';
        $tasks = File::get($taskTemplate);
        return $tasks;
    }

    /**
     * Update the project with the resulting build data
     *
     * @param $extensionName
     * @param $project
     * @param $vcsArray
     * @param $packageInfo
     * @param $tasks
     */
    protected function updateProject($extensionName, $project, $vcsArray, $packageInfo, $tasks)
    {
        $project->name        = $extensionName;
        $project->vcs         = json_encode($vcsArray);
        $project->description = $packageInfo['description'];
        $project->user_id     = 0;
        $project->task_config = $tasks;
        $project->url         = '';

        $project->save();
    }

    /**
     * Parse the package.xml and get connect extension information
     *
     * @param $storagePath
     * @return mixed
     */
    protected function parseConnectPackageInformation($storagePath)
    {
        $packageInfo = Parser::xml(File::get($storagePath . "/package.xml"));
        return $packageInfo;         // XML >  Array
    }

    /**
     * Create and save the buildRequest
     *
     * @param $project
     * @param $buildConfiguration
     */
    protected function createBuildRequest($project, $buildConfiguration)
    {
        $requestedBuild                = new BuildQueue();
        $requestedBuild->status        = 'pending';
        $requestedBuild->project_id    = $project->id;
        $requestedBuild->configuration = json_encode($buildConfiguration);

        $requestedBuild->save();
    }

    /**
     * Generate the extension file path for the connect extension
     *
     * @param $extensionName
     * @param $currentVersion
     * @return string
     */
    protected function getExtensionFilePath($extensionName, $currentVersion)
    {
        $downloadExtension = base_path() . "/tools/magento/downloader/.cache/community/{$extensionName}-{$currentVersion}.tgz";
        return $downloadExtension;
    }

    /**
     * @param $result
     * @return array
     */
    protected function convertListToArray($result)
    {
        $matches = explode("\n", $result);
        return $matches;
    }

    /**
     * @param $result
     * @return array
     */
    protected function parseMagentoConnectList($result)
    {
        $parsedResult  = array();
        $matches       = $this->convertListToArray($result);

        foreach ($matches as $id => $data) {


            $extension     	= explode(":", $data);
            $versions      	= explode(',', $extension[1]);
            $versionData    = explode(' ', trim($versions[0]));

            if (!empty($versionData[0])) {
                $parsedResult[$id] = array(
                    'name'      => $extension[0],
                    'version'   => $versionData[0],
                    'stability' => 'dev'
                );
            }

            if (isset($versionData[1])) {
                $parsedResult[$id]['stability'] = $versionData[1];
            }
        }
        return $parsedResult;
    }

}
