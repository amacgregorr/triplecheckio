<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Indexes extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'build:indexes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //

//        $buildList = Build::where('is_public', true)
//                          ->where('result', '!=', '[]')->get();

        $buildList = Build::where('is_public', true)->get();

        foreach ($buildList as $build) {
            $index = BuildIndex::where("build_id", "=", $build->id)->first();

            if (is_null($index)) {
                $index = new BuildIndex;
            }

            $author = $this->_getAuthor($build->project);

            if(is_null($author))
            {
                $author = 'Unknown';
            }

            $index->build_id    = $build->id;
            $index->author      = $author;
            $index->name        = $build->project->name;
            $index->description = $build->project->description;
            $index->score       = number_format($build->getAverageScore(), 2);
            $index->grade       = calculateGrade(number_format($build->getAverageScore(), 2));
            $index->save();
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//			array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

    protected function _getAuthor($project)
    {
        try {
            $repoDirName = str_replace(" ", "_", strtolower("{$project->name}_connect_{$project->id}"));

            $storagePath = storage_path() . '/builds/repositories/' . $repoDirName;
            $packgeInfo  = Parser::xml(File::get($storagePath . "/package.xml")); // XML > Array

            return $packgeInfo['authors']['author']['name'];
        } catch (Exception $e) {
            return 'N/A';
        }
    }

}
