<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class QueueManager extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'crimson:queue';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
		$arguments = $this->argument();

		if(isset($arguments['list']))
		{
			$this->listProjects();
		}

	}

	protected function listProjects()
	{
		$filter = $this->option('status');

		if (isset($filter)) {
			$projectList = BuildQueue::where('status', '=', $filter)->get();
		} else {
			$projectList = BuildQueue::all;
		}

		foreach ($projectList as $project) {
			$config = json_decode($project->configuration);
			echo "{$config->configuration->project->name} - {$config->configuration->vcs->path} \r\n";
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('list', InputArgument::REQUIRED, 'Retrieve the full list of projects.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('status', null, InputOption::VALUE_OPTIONAL, 'Filter projects by Status', null),
		);
	}

}
