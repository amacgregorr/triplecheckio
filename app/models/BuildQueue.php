<?php

class BuildQueue extends \Eloquent
{
    protected $fillable = [];


    public function project()
    {
        return $this->belongsTo('Project');
    }

    public function markAs($status)
    {
        $this->attributes['status'] = $status;
        $this->save();
    }
}