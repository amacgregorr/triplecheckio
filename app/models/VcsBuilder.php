<?php

class VcsBuilder
{
    public static function create($type)
    {
        $type = "VcsAdapter_".ucfirst($type)."Adapter";
        return new $type();
    }
}