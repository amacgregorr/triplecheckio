<?php

class Build extends \Eloquent
{
    protected $fillable = [];

    public function getAverageScore()
    {
        return json_decode($this->attributes['score_data'])->avg_score;
    }

    public function project()
    {
        return $this->belongsTo('Project');
    }

}