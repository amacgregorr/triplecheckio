<?php

class BuildIndex extends \Eloquent {
	protected $fillable = [];
    protected $table = 'build_indexes';

    public function build()
    {
        return $this->belongsTo('Build');
    }

}