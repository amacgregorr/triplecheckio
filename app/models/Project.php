<?php

/**
 * Class Project
 */
class Project extends \Eloquent
{

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return mixed
     */
    public function newQuery()
    {
        // [Refactor] - This should be handled better at some point
        try {
            $tenancy = App::make('tenancy');
        } catch (Exception $e) {
            $tenancy = new stdClass;
        }
        // End Refactor

        if (isset($tenancy->user_id)) {
            return parent::newQuery()->where('user_id', '=', $tenancy->user_id);
        } else {
            return parent::newQuery();
        }
    }

    /**
     * @return mixed
     */
    public function builds()
    {
        return $this->hasMany('Build');
    }

    /**
     * @return mixed
     */
    public function buildQueues()
    {
        return $this->hasMany('BuildQueue');
    }

    /**
     * @return array
     */
    public function getBuildConfigurationData()
    {
        $vcsData  = json_decode($this->attributes['vcs']);
        $taskData = json_decode($this->attributes['task_config']);

        $buildConfigData = array(
            'configuration' => array(
                'project' => array(
                    'url'         => $this->attributes['url'],
                    'name'        => $this->attributes['name'],
                    'description' => $this->attributes['description'],
                ),
                'vcs'     => $vcsData
            ),
            'tasks'         => $taskData
        );

        return $buildConfigData;
    }

    /**
     * @return bool
     */
    public function getLatestScore()
    {
        $latestBuild = $this->builds()->orderby('id', 'desc')->first();

        if ($latestBuild) {
            return $latestBuild->getAverageScore();
        } else {
            return false;
        }
    }
}