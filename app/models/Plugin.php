<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Plugin extends BaseModel
{
    protected $fillable      = [];
    protected $table         = 'plugins';
    protected $stiClassField = 'class_name';
    protected $stiBaseClass  = 'Plugin';
}