<?php

class VcsAdapter_Abstract
{
    protected $_configuration;
    protected $_isNew = false;

    public function setConfiguration($configuration)
    {
        $this->_configuration = $configuration;
    }

    public function setIsNew($isNew)
    {
        $this->_isNew = $isNew;
    }

    public function validatePath($repoPath)
    {
        if (!File::exists($repoPath)) {
            $this->_isNew = true;
        }
    }

    protected function _getCommand()
    {
        return L4shell::get();

    }
}
