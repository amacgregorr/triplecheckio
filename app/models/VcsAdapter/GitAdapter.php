<?php
/**
 * Created by PhpStorm.
 * User: amacgregor
 * Date: 10/11/14
 * Time: 2:23 PM
 */
class VcsAdapter_GitAdapter extends VcsAdapter_Abstract
    implements VcsAdapter_Interface
{
    public function validateConfiguration()
    {
        // TODO: Implement validateConfiguration() method.
    }

    public function run()
    {
        if ($this->_isNew) {
            // Check dir exists
            $this->_checkout();
        }
        $this->_update();
    }

    protected function _update()
    {

        $command = $this->_getCommand();
        $result = $command->setCommand('cd %s && git pull')
            ->setExecutablePath()
            ->setArguments(
                array(
                    $this->_configuration->destination
                )
            )->sendToDevNull()
            ->execute();
    }

    protected function _checkout()
    {
        $command = $this->_getCommand();
        $url = str_replace("https://", "", $this->_configuration->url);
        $source = "https://{$this->_configuration->username}:{$this->_configuration->password}@{$url}";
        $result = $command->setCommand('git clone %s %s')
            ->setExecutablePath()
            ->setArguments(
                array(
                    $source,
                    $this->_configuration->destination
                )
            )->sendToDevNull()
            ->execute();
    }
}