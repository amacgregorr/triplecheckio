<?php
/**
 * Created by PhpStorm.
 * User: amacgregor
 * Date: 10/11/14
 * Time: 2:23 PM
 */
class VcsAdapter_SvnAdapter extends VcsAdapter_Abstract
    implements VcsAdapter_Interface
{
    public function validateConfiguration()
    {
        // TODO: Implement validateConfiguration() method.
    }

    public function run()
    {

        if ($this->_isNew) {
            // Check dir exists
            $this->_checkout();
        }
        $this->_update();
    }

    protected function _update()
    {
        $command = $this->_getCommand();
        $command->setCommand('svn --username %s --password %s --non-interactive update %s')
            ->setExecutablePath()
            ->setArguments(
                array(
                    $this->_configuration->username,
                    $this->_configuration->password,
                    $this->_configuration->destination
                )
            )->sendToDevNull()
            ->execute();
    }

    protected function _checkout()
    {
        $command = $this->_getCommand();
        $command->setCommand('svn --username %s --password %s --non-interactive checkout %s %s')
            ->setExecutablePath()
            ->setArguments(
                array(
                    $this->_configuration->username,
                    $this->_configuration->password,
                    $this->_configuration->url,
                    $this->_configuration->destination
                )
            )->sendToDevNull()
            ->execute();
    }
}