<?php
/**
 * Created by PhpStorm.
 * User: amacgregor
 * Date: 10/11/14
 * Time: 2:48 PM
 */

interface VcsAdapter_Interface {

    public function setConfiguration($configuration);
    public function validateConfiguration();
    public function setIsNew($isNew);
    public function run();

}