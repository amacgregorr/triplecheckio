<?php
use MagentoRewrites\Inspect as RewriteInspector;

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Plugin_MagentoRewrite extends Plugin_Abstract
    implements Runnable_Interface, Formattable_Interface
{
    const TOOL = 'magerewrite';

    /**
     * @return void
     */
    public function run()
    {
        $tool        = $this::TOOL;

        $configXmlPath = array();

        // Start [Refactor] : This needs to be replaced by more flexible and robust configuration per project
        $this->_setExecutablePath("/opt/tools/bin/");
        // End Refactor

        $buildResult = $this->_path . "/build_{$tool}.json";


        // Get Configuration files
        $dir_iterator = new RecursiveDirectoryIterator($this->_path);
        $iterator     = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);
        foreach ($iterator as $file) {
            $pattern = 'config.xml';
            if (ends_with($file, $pattern)) {
                $configXmlPath[] = $file;
            }
        }

        if (empty($configXmlPath)) {
            return;
        }

        try {
            foreach ($configXmlPath as $configFile) {
                $inspector = new RewriteInspector;

                $contents                 = File::get($configFile);
                $filename                 = str_replace($this->_path . '/', '', $configFile);
                $this->_result[$filename] = $inspector->run($contents);
            }
        } catch (Exception $e) {
            Log::error($contents);

        }

        return;
    }

    public function formatResult()
    {
        $formattedResult = array();

        foreach ($this->_result as $file => $result) {

            $formattedResult['files'][$file][$this::TOOL]['rewrites'] = $result;

        }

        return $formattedResult;
    }
}