<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
interface Scorable_Interface
{
    public function getScoringInstance();
}