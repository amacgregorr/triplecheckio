<?php
/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Plugin_Phpmetrics extends Plugin_Abstract
    implements Runnable_Interface, Scorable_Interface, Formattable_Interface, Configurable_Interface
{
    const TOOL = 'phpmetrics';

    public function run()
    {
        $tool        = $this::TOOL;

        // Start [Refactor] : This needs to be replaced by more flexible and robust configuration per project
        $this->_setExecutablePath("/opt/tools/bin/");
        // End Refactor

        $buildResult = $this->_path . "/build_{$tool}.json";

        File::put($buildResult, '{}');

        $this->_result = $this->_command->setCommand("{$tool} --excluded-dirs=%s --report-json=%s %s")
            ->setArguments(
                array(
                    "lib|skin",
                    $buildResult,
                    $this->_path
                )
            )->sendToDevNull(true)->setAllowedCharacters(array(">"))->execute();

        if (File::exists($buildResult)) {
            $this->_result = json_decode(File::get($buildResult));
        } else {
            throw new Exception('Something went wrong with this build');
        }
    }

    public function formatResult()
    {
        $formattedResult = array();

        foreach ($this->_result as $id => $result) {
            $file = str_replace($this->_path . '/', '', $result->filename);

            unset($result->name);
            unset($result->filename);

            $formattedResult['files'][$file][$this::TOOL]['metrics'] = array();

            foreach ($result as $metric => $score) {
                $formattedResult['files'][$file][$this::TOOL]['metrics'][$metric] = $score;
            }

        }

        return $formattedResult;
    }

    public function setConfiguration($settings)
    {

    }

    public function getScoringInstance()
    {
        return new Scoring_Plugin_Phpmetrics();
    }

}
