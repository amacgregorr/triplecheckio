<?php

/**
 * Created by PhpStorm.
 * User: amacgregor
 * Date: 07/11/14
 * Time: 5:47 AM
 */
abstract class Plugin_Abstract extends Plugin
{
    protected $_result;
    protected $_path;
    protected $_configuration;
    protected $_command;

    public function __construct($attributes = array())
    {
        // Create instance of the command
        $this->_command = L4shell::get();
        $this->_result  = array();

        parent::__construct($attributes);
    }

    protected function _execute($tool, $path, $argumentList)
    {
        $this->_result = $this->_command->setCommand("{$tool} --report=%s --standard=%s --extensions=%s --ignore=%s %s > %s")
            ->setArguments(
                array(
                    $this->_configuration['report_type'],
                    $this->_configuration['standards'],
                    $this->_configuration['extensions'],
                    $this->_configuration['ignore_list'],
                    $path
                )
            )->execute();
    }

    protected function _setExecutablePath($path)
    {
        $this->_command->setExecutablePath($path);

    }

    public function setPath($p)
    {
        $this->_path = $p;

        return $this;
    }

}