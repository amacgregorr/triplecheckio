<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Plugin_Phpcs extends Plugin_Abstract
    implements Runnable_Interface, Scorable_Interface, Formattable_Interface, Configurable_Interface
{
    const TOOL = 'phpcs';

    public function run()
    {
        $tool        = $this::TOOL;

        // Start [Refactor] : This needs to be replaced by more flexible and robust configuration per project
        $this->_setExecutablePath("/opt/tools/bin/");
        // End Refactor

        $buildResult = $this->_path . "/build_{$tool}.json";

        // Start [Refactor] : Move into reusable adapter
        if (!empty($this->_configuration['ignore_list'])) {
            $this->_result = $this->_command->setCommand("{$tool} --report=%s --standard=%s --extensions=%s --ignore=%s %s > %s")
                ->setArguments(
                    array(
                        $this->_configuration['report_type'],
                        $this->_configuration['standards'],
                        $this->_configuration['extensions'],
                        $this->_configuration['ignore_list'],
                        $this->_path,
                        $buildResult
                    )
                )->sendToDevNull(false)->setAllowedCharacters(array(">", "*"))->execute();
        } else {
            $this->_result = $this->_command->setCommand("{$tool} --report=%s --standard=%s --extensions=%s %s > %s")
                ->setArguments(
                    array(
                        $this->_configuration['report_type'],
                        $this->_configuration['standards'],
                        $this->_configuration['extensions'],
                        $this->_path,
                        $buildResult
                    )
                )->sendToDevNull(false)->setAllowedCharacters(array(">"))->execute();
        }
        // End Refactor

        if (File::exists($buildResult)) {
            $this->_result = json_decode(File::get($buildResult));

            if (is_null($this->_result)) {
                throw new Exception("Result File {$buildResult} corrupted or missing");
            }

        } else {
            throw new Exception('Something went wrong with this build');
        }
    }

    public function formatResult()
    {
        $formattedResult = array();

        foreach ($this->_result->files as $file => $result) {
            $file = str_replace($this->_path . '/', '', $file);


            $formattedResult['files'][$file][$this::TOOL]['issues'] = array();

            // Set the file path
            $formattedResult['files'][$file][$this::TOOL]['issue_summary'] = array(
                'errors'   => $result->errors,
                'warnings' => $result->warnings
            );

            // Process each of the errors
            foreach ($result->messages as $issue) {
                $formattedResult['files'][$file][$this::TOOL]['issues'][] = array(
                    'type'     => strtolower($issue->type),
                    'line'     => $issue->line,
                    'severity' => $issue->severity,
                    'message'  => $issue->message,
                );
            }
        }

        return $formattedResult;
    }

    public function setConfiguration($settings)
    {

        if (isset($settings)) {
            $this->_configuration = array(
                'report_type' => $this->getReportType($settings->report),
                'standards'   => $this->getStandardsList($settings->standards),
                'extensions'  => $this->getFileExtensionList($settings->file_extensions),
                'ignore_list' => $this->getIgnoreList($settings->ignore_list),
            );
        }
    }

    protected function getStandardsList($standards = null)
    {
        if (!is_null($standards)) {
            return implode(',', $standards);
        }
    }

    protected function getFileExtensionList($extensions = null)
    {
        if (!is_null($extensions)) {
            return implode(',', $extensions);
        }
    }

    protected function getIgnoreList($ignoreFiles = null)
    {
        $fileList = array();

        if (!is_null($ignoreFiles) && !empty($ignoreFiles)) {
            foreach ($ignoreFiles as $file) {
                if ($file->type == 'directory') {
                    $fileList[] = $this->_path . '/' . $file->path . '*.*';
                } else {
                    $fileList[] = $this->_path . '/' . $file->path;
                }
            }

        }

        return implode(',', $fileList);
    }

    protected function getReportType($type = null)
    {
        if (!is_null($type)) {
            return $type;
        }
        return 'json';
    }

    protected function _calculateFileScore($issueList)
    {
        $score = 0;
        foreach ($issueList as $issue) {
            if ($issue['type'] == 'error') {
                $score += 5;
            }

            if ($issue['type'] == 'warning') {
                $score += 1;
            }
        }

        if ($score >= 5) {
            return 0;
        }

        return (5 - $score);
    }

    public function getScoringInstance()
    {
        return new Scoring_Plugin_Phpcs();
    }
}