<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Scoring_Plugin_Phpcs extends Scoring_Plugin_Abstract
{
    protected $code = 'phpcs';

    public function run($pluginData, $rules = array())
    {
        if (empty($rules)) {
            $rules = $this->getRules();
        }
        $this->scoreFile($pluginData, $rules);
        return $this->score;
    }

    public function scoreFile($pluginData, $rules)
    {
        try {
            foreach ($rules as $ruleCode => $rule) {
                $rule->setData($pluginData);
                $this->score[$ruleCode] = $rule->getScore();
            }
        } catch (Exception $e) {
            echo "{$e->getMessage()} \r\n";
            unset($rules[$ruleCode]);
            $this->run($pluginData, $rules);
        };
    }


}
