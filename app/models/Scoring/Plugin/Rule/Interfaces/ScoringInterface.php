<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
interface Scoring_Rule_Interfaces_ScoringInterface
{
    public function getScore();
    public function setData($pluginData);
}
