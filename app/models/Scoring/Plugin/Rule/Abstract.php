<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */

class Scoring_Plugin_Rule_Abstract
{
    protected $data;
    protected $code;

    public function setData($pluginData)
    {
        $this->data = $pluginData;
    }

    public function getCode()
    {
        return $this->code;
    }
}
