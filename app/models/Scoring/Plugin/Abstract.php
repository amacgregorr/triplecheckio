<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Scoring_Plugin_Abstract
{
    protected $score = array();

    public function getRules()
    {
        $ruleList      = array();
        $directoryPath = $this->getDirectoryPath();

        $files = File::allFiles($directoryPath);

        foreach ($files as $file) {
            $rule                               = $this->getRuleClassname($file);
            $ruleInstance                       = new $rule;
            $ruleList[$ruleInstance->getCode()] = $ruleInstance;
        }

        return $ruleList;
    }

    /**
     * @return string
     */
    protected function getDirectoryPath()
    {
        $directoryPath = app_path() . "/models/Scoring/Plugin/" . Str::title($this->code) . "/Rule/";
        return $directoryPath;
    }

    /**
     * @param $file
     */
    protected function getRuleClassname($file)
    {
        $classname  = str_replace(".php", "", $file->getFilename());
        $pluginName = Str::title($this->code);

        return "Scoring_Plugin_{$pluginName}_Rule_{$classname}";
    }
}
