<?php
/**
 * Class per file
 *
 * Check the file to make sure no more than one class is define in it.
 *
 * If more than one class is defined on a single mark the file as an automatic failure.
 */

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */

class Scoring_Plugin_Phpmetrics_Rule_NocChecks extends Scoring_Plugin_Rule_Abstract
    implements Scoring_Rule_Interfaces_ScoringInterface
{
    protected $code = 'nocchecks';

    public function getScore()
    {
        if (!isset($this->data)) {
            throw new Exception("No Data Set, please make sure the plugin ran in the first place");
        }

        if (!isset($this->data['metrics']['noc'])) {
            throw new Exception("The plugin data didn't contain a noc score.");
        }

        return $this->calculateScore();
    }

    protected function calculateScore()
    {
        if ($this->data['metrics']['noc'] > 1) {
            return 0;
        }
        if ($this->data['metrics']['noca'] > 1) {
            return 0;
        }
        if ($this->data['metrics']['nocc'] > 1) {
            return 0;
        }

        return 5;
    }
}