<?php
/**
 * Class per file
 *
 * Check the file to make sure no more than one class is define in it.
 *
 * If more than one class is defined on a single mark the file as an automatic failure.
 */

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */

class Scoring_Plugin_Phpmetrics_Rule_Lloc extends Scoring_Plugin_Rule_Abstract
    implements Scoring_Rule_Interfaces_ScoringInterface
{
    protected $code = 'lloc';

    public function getScore()
    {
        if (!isset($this->data)) {
            throw new Exception("No Data Set, please make sure the plugin ran in the first place");
        }

        if (!isset($this->data['metrics']['logicalLoc'])) {
            throw new Exception("The plugin data didn't contain a lloc score.");
        }

        return $this->calculateScore();
    }

    protected function calculateScore()
    {
        $metric = $this->data['metrics']['logicalLoc'];

        if ($metric >= 500) {
            return -1;
        }

        if ($metric < 500 && $metric >= 400) {
            return 1;
        }

        if ($metric < 400 && $metric >= 300) {
            return 2;
        }

        if ($metric < 300 && $metric >= 200) {
            return 3;
        }

        if ($metric < 200 && $metric >= 100) {
            return 4;
        }

        if ($metric < 100 && $metric > 0) {
            return 5;
        }

        return 0;
    }
}