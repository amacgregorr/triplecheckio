<?php

/**
 * Maintainability Index
 *
 * Score the maintainability index returned from the PHPmetrics report.
 *
 * The maintainability index is based from the LOC and Cyclomatic complexity number.
 *
 * Scoring:
 * Maintinability value divided by 20
 *
 */

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Scoring_Plugin_Phpmetrics_Rule_Maintainability extends Scoring_Plugin_Rule_Abstract
    implements Scoring_Rule_Interfaces_ScoringInterface
{
    protected $code = 'maintainability';
    protected $scoreFactor = 20;

    public function getScore()
    {
        if (!isset($this->data)) {
            throw new Exception("No Data Set, please make sure the plugin ran in the first place");
        }

        if (!isset($this->data['metrics']['maintenabilityIndex'])) {
            throw new Exception("The plugin data didn't contain a maintenabilityIndex score.");
        }

        return $this->calculateScore();
    }

    protected function calculateScore()
    {
        $score = number_format($this->data['metrics']['maintenabilityIndex'] / $this->scoreFactor, 2);

        if ($score > 5) {
            return 5;
        }

        return $score;
    }
}
