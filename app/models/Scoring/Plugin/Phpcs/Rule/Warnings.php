<?php

/**
 * Maintainability Index
 *
 * Score the maintainability index returned from the PHPmetrics report.
 *
 * The maintainability index is based from the LOC and Cyclomatic complexity number.
 *
 * Scoring:
 * Maintinability value divided by 20
 *
 */

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Scoring_Plugin_Phpcs_Rule_Warnings extends Scoring_Plugin_Rule_Abstract
    implements Scoring_Rule_Interfaces_ScoringInterface
{
    protected $code = 'warnings';

    public function getScore()
    {
        if (!isset($this->data)) {
            throw new Exception("No Data Set, please make sure the plugin ran in the first place");
        }

        if (!isset($this->data['issue_summary'])) {
            throw new Exception("The plugin data didn't contain a issues.");
        }

        return $this->calculateScore();
    }

    protected function calculateScore()
    {

        if (!isset($this->data['issue_summary']['warnings']) || $this->data['issue_summary']['warnings'] == 0) {
            return 5;
        }

        $warnings = $this->data['issue_summary']['warnings'];
        $score  = 5 - (0.5 * $warnings);

        return $score;
    }
}
