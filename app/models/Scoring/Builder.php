<?php

/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 */
class Scoring_Builder
{

    public function run($buildResults)
    {
        $results              = array();
        $fileAverageScoreList = array();

        if (!array_key_exists('files', $buildResults)) {
            $results['avg_score'] = 0.0;
            return $results;
        }

        foreach ($buildResults['files'] as $file => $plugin) {
            $pluginAverageScoreList = array();

            foreach ($plugin as $pluginCode => $pluginData) {
                $pluginInstance = Plugin::where('code', $pluginCode)->first();

                if (!$pluginInstance instanceof Scorable_Interface) {
                    continue;
                }

                $scoringInstance = $pluginInstance->getScoringInstance();
                $fileScore       = $scoringInstance->run($pluginData);

                $results[$file][$pluginCode]         = $fileScore;
                $pluginAverageScoreList[$pluginCode] = $this->calculateAverageScore($fileScore);
            }

            if (empty($pluginAverageScoreList)) {
                continue;
            }

            $fileAverageScore = $this->calculateAverageScore($pluginAverageScoreList);

            $fileAverageScoreList[]      = $fileAverageScore;
            $results[$file]['avg_score'] = $fileAverageScore;
        }

        $results['avg_score'] = $this->calculateAverageScore($fileAverageScoreList);
        return $results;
    }

    /**
     * @param $scoreList
     * @return int
     */
    protected function calculateAverageScore($scoreList)
    {
        try {
            $compoundScore = 0;
            foreach ($scoreList as $key => $value) {
                $compoundScore += $value;
            }
            $compoundScore = $compoundScore / count($scoreList);

            return $compoundScore;
        } catch (Exception $e) {
            return 0;
        }
    }
}
