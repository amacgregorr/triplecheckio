<?php

class WebhookController extends \BaseController
{

    public function trigger($id)
    {
        $params = Input::all();

        $project = Project::where('project_token', $id)->first();


        // Prepare the project configuration data
        $buildConfiguration = $project->getBuildConfigurationData();

        //Create the build object

        $requestedBuild = new BuildQueue();

        $requestedBuild->status        = 'pending';
        $requestedBuild->project_id    = $project->id;
        $requestedBuild->configuration = json_encode($buildConfiguration);

        $requestedBuild->save();
    }

}