<?php

class DashboardBaseController extends BaseController
{
    public function __construct()
    {
        // this function will run before every action in the controller
        $this->beforeFilter(function () {
            // Singleton (global) object
            App::singleton('tenancy', function () {
                $app = new stdClass;
                if (Auth::check()) {
                    // Put your User object in $app->user
                    $app->user_id = Auth::user()->id;
                }
                return $app;
            });

            $projects = Project::all();
            View::share('projects', $projects);
        });
    }
}
