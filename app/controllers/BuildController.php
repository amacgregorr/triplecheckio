<?php

class BuildController extends DashboardBaseController
{

	/**
	 * Display a listing of the resource.
	 * GET /build
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /build/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /build
	 *
	 * @throws Exception
	 * @return Response
	 */
	public function store()
	{
		// Check that the parameters are set
		$params = Input::all();
		if (!isset($params['project_id'])) {
			throw new Exception('something went wrong');
		}

		// Load the project
		$project = Project::findOrFail($params['project_id']);

		// Prepare the project configuration data
		$buildConfiguration = $project->getBuildConfigurationData();

		//Create the build object

		$requestedBuild = new BuildQueue();

		$requestedBuild->status        = 'pending';
		$requestedBuild->project_id    = $project->id;
		$requestedBuild->configuration = json_encode($buildConfiguration);

		$requestedBuild->save();
	}

	/**
	 * Display the specified resource.
	 * GET /build/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /build/{id}/edit
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /build/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /build/{id}
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}