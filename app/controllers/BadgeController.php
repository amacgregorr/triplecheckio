<?php

class BadgeController extends \BaseController {

	/**
	 * Display the specified resource.
	 * GET /badge/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($type, $id)
	{
		// Validate the project_id
        $currentProject = Project::find($id);

        // Get the last available build
        $latestBuild = $currentProject->builds()->orderby('id', 'desc')->first();

        // Get the Build index
        $indexData = BuildIndex::where('build_id',$latestBuild->id)->first();


        $colors = array(
            "A+" => "brightgreen",
            "A" => "green",
            "B" => "yellow",
            "C" => "orange",
            "D" => "red",
            "F" => "red",
        );
        $status = "Grade {$indexData->grade}";
        if($type == 'score'){
            $status = "Score {$indexData->score}";
        }

        $client = new GuzzleHttp\Client();

        //https://img.shields.io/badge/<SUBJECT>-<STATUS>-<COLOR>.svg
        $url = "https://img.shields.io/badge/triplecheck.io-{$status}-{$colors[$indexData->grade]}.svg";

        $res = $client->get($url);

        $statusCode = $res->getStatusCode();
        $contentType = $res->getHeader('content-type');
        $contents = $res->getBody();


        $response = Response::make($contents, $statusCode);

        $response->header('Content-Type', $contentType);

        return $response;

	}


}