<?php

class ConnectController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /projects
     *
     * @return Response
     */
    public function index()
    {

        $allowed_columns = ['name', 'author', 'score'];
        $sort            = in_array(Input::get('sort'), $allowed_columns) ? Input::get('sort') : 'name';
        $order           = Input::get('order') === 'desc' ? 'desc' : 'asc';
        $keyword         = Input::get('keyword');


        if (!empty($keyword)) {
            $buildList = BuildIndex::where('name', 'LIKE', '%' . $keyword . '%')
                                   ->orderBy($sort, $order);

        } else {
            $buildList = BuildIndex::orderBy($sort, $order);
        }

        $buildList = $buildList->paginate(100);

        return View::make('connect.list', array(
            'builds' => $buildList,
            'sort' => $sort,
            'keyword' => $keyword,
            'order' => $order
        ));
    }

    public function search()
    {
        $input = Input::all();

        unset($input['_token']);

        return Redirect::action('ConnectController@index', $input);
    }

    public function report()
    {
        $analyzed = DB::select(DB::raw('select count(*) as count from build_indexes'));
        $total    = DB::select(DB::raw('SELECT count(*) as count from projects WHERE user_id =0'));
        $scores   = DB::select(DB::raw('select grade,score,count(*) as count from build_indexes GROUP BY grade order by score desc'));


        $allowed_columns = ['name', 'author', 'score'];
        $sort            = in_array(Input::get('sort'), $allowed_columns) ? Input::get('sort') : 'name';
        $order           = Input::get('order') === 'desc' ? 'desc' : 'asc';
        $keyword         = Input::get('keyword');


        //        $buildList = BuildIndex::all();


        if (!empty($keyword)) {
            $buildList = BuildIndex::where('name', 'LIKE', '%' . $keyword . '%')
                                   ->orderBy($sort, $order);

        } else {
            $buildList = BuildIndex::orderBy($sort, $order);
        }

        $buildList = $buildList->paginate(100);

        return View::make('connect.report', array(
            'builds' => $buildList,
            'sort' => $sort,
            'keyword' => $keyword,
            'order' => $order,
            'scores' => $scores,
            'analyzed' => $analyzed[0]->count,
            'total' => $total[0]->count
        ));


    }

    public function view($id)
    {
        // load project
        $currentProject = Project::find($id);
        $buildData      = array(
            'issues' => null,
            'score_data' => null
        );

        View::share('currentProject', $currentProject);

        // Set the breadcrumbs path
        $breadcrumbs = array('Projects' => '/dashboard/projects');
        if (isset($currentProject->id)) {
            $breadcrumbs[$currentProject->name] = '';
        } else {
            $breadcrumbs['Not Found'] = '';
        }
        View::share('breadcrumbs', $breadcrumbs);

        //Get the last available build
        $latestBuild = $currentProject->builds()->orderby('id', 'desc')->first();

        if ($latestBuild && $latestBuild->is_public) {
            $buildData['score_data'] = json_decode($latestBuild->score_data);
            // Start [Refactor] : Change to load with new data structure for multiple plugins

            $results  = json_decode($latestBuild->result);
            $rewrites = array();

            $buildData['issues'] = array();


            foreach ($results->files as $file => $data) {
                foreach ($data as $plugin => $pluginData) {
                    if ($plugin == 'magerewrite') {
                        $rewrites = $data;
                        continue;
                    }
                    if (isset($pluginData->issues)) {
                        foreach ($pluginData->issues as $issue) {
                            $buildData['issues'][] = array(
                                'type' => $issue->type,
                                'line' => $issue->line,
                                'severity' => $issue->severity,
                                'message' => $issue->message,
                                'file' => $file,
                                'plugin' => $plugin
                            );
                        }
                    }
                }
            }



//            foreach ($results as $plugin => $data) {
//                if ($plugin == 'magerewrite') {
//                    $rewrites = $data;
//                    continue;
//                }
//                foreach ($data->files as $file => $fileData) {
//                    foreach ($fileData->issues as $issue) {
//                        $buildData['issues'][] = array(
//                            'type' => $issue->type,
//                            'line' => $issue->line,
//                            'severity' => $issue->severity,
//                            'message' => $issue->message,
//                            'file' => $file,
//                            'plugin' => $plugin
//                        );
//                    }
//                }
//            }
            // End Refactor
        } else {
            return false;
        }

        $payload = array(
            'project' => $currentProject,
            'issueList' => $buildData['issues'],
            'scoreData' => $buildData['score_data']
        );

        if (isset($rewrites->files)) {
            $payload['rewrites'] = $rewrites->files;
        } else {
            $payload['rewrites'] = null;

        }


        return View::make('connect.list.view', $payload);

    }

    /**
     * Show the form for creating a new resource.
     * GET /projects/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /projects
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /projects/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /projects/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /projects/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /projects/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}