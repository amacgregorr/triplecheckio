<?php

class ProjectsController extends \DashboardBaseController
{

    /**
     * Display a listing of the resource.
     * GET /projects
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /projects/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /projects
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /projects/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /projects/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /projects/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $project = Project::find($id);
        $fields = Input::all();

		foreach($fields as $key => $data)
		{
			if ($key != '_token') {
				$project->$key = $data;
			}
		}

        $project->save();

        return Redirect::back();
    }

    public function view($id)
    {
        // load project
        $currentProject = Project::find($id);
        $buildData      = array(
            'issues'     => array(),
            'score_data' => array(),
			'issue_count'=> array()
        );

        View::share('currentProject', $currentProject);

        // Set the breadcrumbs path
        $breadcrumbs = array('Projects' => '/dashboard/projects');
        if (isset($currentProject->id)) {
            $breadcrumbs[$currentProject->name] = '';
        } else {
            $breadcrumbs['Not Found'] = '';
        }
        View::share('breadcrumbs', $breadcrumbs);

        //Get the last available build
        $latestBuild = $currentProject->builds()->orderby('id', 'desc')->first();
        if ($latestBuild) {
            $buildData['score_data'] = json_decode($latestBuild->score_data);
            // Start [Refactor] : Change to load with new data structure for multiple plugins

            $results = json_decode($latestBuild->result);

            $buildData['issues'] = array();

            foreach ($results as $plugin => $data) {
                foreach ($data->files as $file => $fileData) {
                    foreach ($fileData->issues as $issue) {

                        if ( isset($buildData['issue_count'][$issue->message])){
                            $buildData['issue_count'][$issue->message]['count']++;
                            $buildData['issue_count'][$issue->message]['file_list'][] = array(
                                'line'     => $issue->line,
                                'file'     => $file
                            );
                        }else {
                            $buildData['issue_count'][$issue->message] = array(
                                "message" => $issue->message,
                                "count"   => 1,
                                "file_list" => array(
                                    0 => array(
                                        'line'     => $issue->line,
                                        'file'     => $file
                                    )
                                )
                            );
                        }

                        $buildData['issues'][] = array(
                            'type'     => $issue->type,
                            'line'     => $issue->line,
                            'severity' => $issue->severity,
                            'message'  => $issue->message,
                            'file'     => $file
                        );
                    }
                }
            }
            // End Refactor

			usort($buildData['issue_count'], function ($a, $b) {
				return $a['count'] - $b['count'];
			});

			$buildData['issue_count'] = array_reverse($buildData['issue_count']);

        }



        return View::make('dashboard.projects.view', array(
            'project'   => $currentProject,
//			'issueList' => array_splice($buildData['issues'] , 0, 10),
			'issueList' => $buildData['issues'],
            'issueCount'=> $buildData['issue_count'],
            'scoreData' => $buildData['score_data']
        ));
    }

    public function editBuild($id)
    {

        $currentProject = Project::find($id);

        // Set the breadcrumbs path
        $breadcrumbs = array('Projects' => '/dashboard/projects');

        if (isset($currentProject->id)) {
            $breadcrumbs[$currentProject->name] = "/dashboard/project/{$id}";
        } else {
            $breadcrumbs['Not Found'] = '';
        }
        $breadcrumbs['Build Settings'] = "";

        View::share('breadcrumbs', $breadcrumbs);

        return View::make('dashboard.projects.edit.build', array('project' => $currentProject));
    }

    public function editSettings($id)
    {

        $currentProject = Project::find($id);

        // Set the breadcrumbs path
        $breadcrumbs = array('Projects' => '/dashboard/projects');

        if (isset($currentProject->id)) {
            $breadcrumbs[$currentProject->name] = "/dashboard/project/{$id}";
        } else {
            $breadcrumbs['Not Found'] = '';
        }
        $breadcrumbs['Project Settings'] = "";

        View::share('breadcrumbs', $breadcrumbs);

        return View::make('dashboard.projects.edit.settings', array('project' => $currentProject));
    }


    /**
     * Remove the specified resource from storage.
     * DELETE /projects/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}