<?php

class SessionsController extends \BaseController
{

    /**
     * Show the form for creating a new resource.
     * GET /sessions/create
     *
     * @return Response
     */
    public function create()
    {
        return View::make('sessions.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /sessions
     *
     * @return Response
     */
    public function store()
    {
        $fields = Input::all();

        $attempt = Auth::attempt([
            'email'    => $fields['email'],
            'password' => $fields['password']
        ]);

        if ($attempt) {
            Session::flash('message', 'You have successfully logged in!');
            return Redirect::to('/dashboard')->with('message' . 'You have successfully logged in!');
        }

        Session::flash('message', 'There was a problem login you in!');
        return Redirect::to('/')->with('message' . 'There was a problem login you in!');

    }

    /**
     * Remove the specified resource from storage.
     * DELETE /sessions/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {
        Auth::logout();
        return Redirect::to('/');
    }

}