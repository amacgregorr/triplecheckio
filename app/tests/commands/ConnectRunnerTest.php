<?php
/**
 * @author Allan MacGregor - Magento Practice Lead <amacgregor@demacmedia.com>
 * @company Demac Media Inc.
 * @copyright 2010-2014 Demac Media Inc.
 * @covers ConnectRunner
 */

class ConnectRunnerTest extends TestCase {

    /**
     * @covers ConnectRunner::checkIfRepositoryDirectoryExists
     */
    public function testCheckIfRepositoryDirectoryExists()
    {
        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testExtensionInformationParser()
    {
        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testGetExtensionList()
    {

        $runner = new ConnectRunner;

        $list = $this->invokeMethod($runner, 'getExtensionList');

        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }



    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
} 