@extends('layouts.dashboard')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="block-flat project-table">
          <div class="header">
            <h3>Project List</h3>
          </div>
          <div class="content">
            <div class="table-responsive">
            <table class="no-border hover list project-list">
            <thead>
                <th>Id</th>
                <th>Project Name</th>
                <th>Project Description</th>
                <th>Score</th>
                <th>Grade</th>
            </thead>
              <tbody class="no-border-y">
                @foreach($projects as $project)
                <tr class="items">
                  <td class="text-left"><strong>{{ $project->id }}</strong></td>
                  <td class="text-left">
                   <strong>{{ link_to_action('ProjectsController@view', $project->name, $parameters = array("id" => $project->id), $attributes = array()); }}</strong>
                  </td>
                  <td class="text-left"><p>{{ $project->description }}</p></td>
                  <td class="text-left">
                    <div class="chart_holder">
                        <div class="chart_labels"><p><span class="text-left color-{{ getGradeColor(number_format($project->getLatestScore(),2)) }}">{{ number_format($project->getLatestScore(),2) }}</span></div>
                        {{--<div class="progress">--}}
                            {{--<div class="progress-bar progress-bar-success" style="width: 65%"></div>--}}
                            {{--<div class="progress-bar progress-bar-warning" style="width: 20%"></div>--}}
                            {{--<div class="progress-bar progress-bar-danger" style="width: 15%"></div>--}}
                        {{--</div>--}}
                    </div>
                  </td>
                  <td class="text-center grade">{{calculateGradeLabel($project->getLatestScore())}}</td>

                </tr>
                @endforeach
              </tbody>
            </table>
            </div>
          </div>
        </div>
    </div>
    {{--<div class="col-md-5">--}}
        {{--<div class="block-flat">--}}
            {{--<div class="header">--}}
                {{--<h3>Alerts</h3>--}}
            {{--</div>--}}
            {{--<div class="content">--}}
                {{--<div class="alert alert-success alert-white rounded">--}}
                    {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
                    {{--<div class="icon"><i class="fa fa-check"></i></div>--}}
                   {{--<strong>Build [#276]</strong> of Snugglebugz was successful.--}}
                 {{--</div>--}}
                 {{--<div class="alert alert-danger alert-white rounded">--}}
                    {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
                    {{--<div class="icon"><i class="fa fa-times-circle"></i></div>--}}
                    {{--<strong>Build [#136]</strong> of Ardene failed.--}}
                 {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>
@stop
