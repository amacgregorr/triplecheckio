@extends('layouts.dashboard')
@section('content')
<h1>Edit Project Settings</h1>

{{ Form::model($project, array('route' => array('project.update', $project->id))) }}
<div class="form-group">
    {{ Form::label('name','Name:') }}
    {{ Form::text('name', $project->name, array('class' => 'form-control')) }}
</div>
<div class="form-group">
    {{ Form::label('url','Url:') }}
    {{ Form::text('url', $project->url, array('class' => 'form-control')) }}
</div>
<div class="form-group">
    {{ Form::label('description','Description:') }}
    {{ Form::textarea('description', $project->description, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('vcs','Version Control Configuration:') }}
    {{ Form::textarea('vcs', $project->vcs, array('class' => 'form-control')) }}
</div>

<button type="submit" class="btn btn-default">Submit</button>

{{ Form::close() }}


<script>
    window.addEventListener("load", editorInit, false);
    function editorInit(e) {
    $(document).ready(function() {
                var value = JSON.parse($('#vcs').val())
                $('#vcs').val(JSON.stringify(value,null,"\t"));

            var editor = ace.edit("vcs");
            editor.setTheme("ace/theme/monokai");
            editor.getSession().setMode("ace/mode/javascript");
            editor.getSession().setTabSize(2);
            editor.getSession().setUseWrapMode(true);
            editor.setOptions({
                maxLines: 55
            });
      });
    };
</script>
TODO:
<br/><br/>
- Delete Option ala github <br/>

@stop