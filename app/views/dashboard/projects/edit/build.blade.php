@extends('layouts.dashboard')
@section('content')
<h1>Edit Build Configuration</h1>

{{ Form::model($project, array('route' => array('project.update', $project->id))) }}

<div class="form-group">
    {{ Form::label('task_config','Build Configuration:') }}
    {{ Form::textarea('task_config', $project->task_config, array('class' => 'form-control editor')) }}
    <div id="task_config_editor"></div>
</div>

<button type="submit" class="btn btn-default">Submit</button>

{{ Form::close() }}

<script>
    window.addEventListener("load", editorInit, false);
    function editorInit(e) {
    $(document).ready(function() {
            var textarea = $('#task_config');

            var value = JSON.parse(textarea.val())
            textarea.val(JSON.stringify(value,null,"\t"));

            textarea.hide();

            var editor = ace.edit("task_config_editor");
            editor.setTheme("ace/theme/monokai");
            editor.getSession().setMode("ace/mode/javascript");
            editor.getSession().setTabSize(2);
            editor.getSession().setUseWrapMode(true);
            editor.getSession().setValue(textarea.val());
            editor.getSession().on('change', function(){
                textarea.val(editor.getSession().getValue());
            });
            editor.setOptions({
                maxLines: 45
            });
      });
    };
</script>

@stop