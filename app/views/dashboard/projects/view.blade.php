@extends('layouts.dashboard')
@section('content')

@if(isset($project->id))
    <div class="row">
        <div class="col-md-8">
            <div class="block block-color project-table">
                <div class="header">
                    <div class="actions">
                        <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                        <a class="refresh" href="#"><i class="fa fa-repeat"></i></a>
                    </div>
                    <h3>Latest Issues</h3>
                </div>
                <div class="content">
                    @if(isset($issueList))
                    <table class="no-border">
                        <thead class="no-border">
                            <tr>
                                <th class="text-left">Plugin</th>
                                <th class="text-center">Severity</th>
                                <th style="width:90%;">Issue</th>
                            </tr>
                        </thead>
                        <tbody class="no-border-y">
                            @foreach($issueList as $issue)
                                <tr>
                                    <td class="text-left">{{ $issue['plugin'] or 'N/A'}}</td>
                                    <td class="text-center"><span class="label label-error">{{ $issue['severity'] }}</span></td>
                                    <td><strong>{{ $issue['message'] }}</strong><br>{{ Str::title($issue['type']) }} in Line {{ $issue['line'] }} on file {{ $issue['file'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h4>No Issues Detected</h4>
                    @endif
                </div>
            </div>
            {{--<div class="block block-color project-table">--}}
                {{--<div class="header">--}}
                    {{--<div class="actions">--}}
                        {{--<a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>--}}
                        {{--<a class="refresh" href="#"><i class="fa fa-repeat"></i></a>--}}
                    {{--</div>--}}
                    {{--<h3>Issue Summary</h3>--}}
                {{--</div>--}}
                {{--<div class="content">--}}
                    {{--@if(isset($issueCount))--}}
                    {{--<table class="no-border">--}}
                        {{--<tbody class="no-border-y">--}}
                            {{--@foreach($issueCount as $issue)--}}
                                {{--<tr>--}}
                                    {{--<td class="text-left">--}}
                                        {{--<div class="block block-color issue-porlet danger ">--}}
                                            {{--<div class="header">--}}
                                                {{--<div class="actions">--}}
                                                    {{--<a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>--}}
                                                {{--</div>--}}
                                                {{--<p><strong>{{ $issue['message'] or 'N/A'}}</strong></p>--}}
                                            {{--</div>--}}
                                            {{--<div class="content">--}}
                                                {{--<table class="no-border">--}}
                                                    {{--<thead class="no-border">--}}
                                                        {{--<tr>--}}
                                                            {{--<th class="text-left">Line</th>--}}
                                                            {{--<th class="text-left">Path</th>--}}
                                                        {{--</tr>--}}
                                                    {{--</thead>--}}
                                                    {{--<tbody class="no-border-y">--}}
                                                        {{--@foreach($issue['file_list'] as $file)--}}
                                                        {{--<tr>--}}
                                                            {{--<td class="text-left">{{$file['line']}}</td>--}}
                                                            {{--<td class="text-left">{{$file['file']}}</td>--}}
                                                        {{--</tr>--}}
                                                        {{--@endforeach--}}
                                                  {{--</tbody>--}}
                                                {{--</table>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                    {{--<td class="text-center"><span class="label label-error">{{ $issue['count'] }}</span></td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                    {{--<script type="text/javascript">--}}
                        {{--window.addEventListener("load", hidePortlets, false);--}}
                        {{--function hidePortlets(e) {--}}
                        {{--$(document).ready(function(){--}}
                            {{--$('.issue-porlet').each(function(index, value) {--}}
                               {{--var c = $(this).children('.content');--}}
                               {{--c.slideToggle();--}}
                               {{--$(this).toggleClass('closed');--}}
                            {{--});--}}
                        {{--});--}}
                        {{--}--}}
                    {{--</script>--}}
                    {{--@else--}}
                        {{--<h4>No Issues Detected</h4>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="block block-color">
                <div class="header">
                    <div class="actions">
                        <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                        <a class="refresh" href="#"><i class="fa fa-repeat"></i></a>
                    </div>
                    <h3>Build History</h3>
                </div>
                <div class="content">
                    @if($project->builds)
                    <table class="no-border">
                        <thead class="no-border">
                            <tr>
                                <th style="width:50%;">Build #</th>
                                <th>Date</th>
                                <th class="text-right">Code Grade</th>
                                <th class="text-right">Score</th>
                            </tr>
                        </thead>
                        <tbody class="no-border-y">
                            @foreach($project->builds as $build)
                            <tr>
                                <td style="width:30%;">{{ $build->id }}</td>
                                <td>{{ $build->created_at }}</td>
                                <td class="text-right">{{calculateGradeLabel($project->getLatestScore())}}</td>
                                <td class="text-right"><span class="color-{{ getGradeColor(number_format($build->getAverageScore(),2)) }}">{{ number_format($build->getAverageScore(),2) }}</span></td>
                            </tr>
                            @endforeach
                        </tbody>
                        @else
                            <h2>No Builds yet</h2>
                        @endif
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block-flat dark-box">
                <div class="header">
                    <div class="actions">
                    <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                    {{--<a class="close-down" href="#"><i class="fa fa-times"></i></a>--}}
                    </div>
                    <h3>{{ $project->name }} </h3>
                </div>
                <div class="content">
                    <p>{{ $project->description }}</p>
                    <p>Token: <br/> <span style="font-size: 10px">{{ $project->project_token }}</span></p>
                </div>
            </div>
            <div class="block-flat dark-box visitors">
                @if(!empty($scoreData))
                    <h4 class="no-margin">Code Score : <span class="color-{{ getGradeColor(number_format($scoreData->avg_score,2)) }}">{{ number_format($scoreData->avg_score,2) }}</span></h4>
                    <hr/>
                    <div class="row">
                        <div class="counters col-md-4">
                            <table class="no-border">
                                <tbody class="no-border-x">
                                    @foreach($scoreData->breakdown as $score)
                                    <tr>
                                        <td><h4 class="color-{{ $score->type }}">{{ Str::title($score->type) }}</h4></td>
                                        <td><h4 class="color-{{ $score->type }}">{{ $score->score }}</h4></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-8">
                            <div id="ticket-chart" style="height: 140px;"></div>
                        </div>
                    </div>
                @else
                    <h3>Score Data Not Available</h3>
                @endif
            </div>

            <div class="block block-color">
                <div class="header">
                    <div class="actions">
                        <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                        <a class="refresh" href="#"><i class="fa fa-repeat"></i></a>
                    </div>
                    <h3>Build Queue</h3>
                </div>
                <div class="content">
                    <table class="no-border">
                        <thead class="no-border">
                            <tr>
                                <th style="width:50%;">Build #</th>
                                <th>Status</th>
                                <th>Requested</th>
                            </tr>
                        </thead>
                        <tbody class="no-border-x">
                            @foreach($project->buildQueues as $entry)

                            <tr>
                                <td style="width:30%;">{{ $entry->id }}</td>
                                <td>{{ $entry->status }}</td>
                                <td>{{ date("Y-m-d", strtotime($entry->created_at)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        window.addEventListener("load", buildRunner, false);
        function buildRunner(e) {
            $(document).ready(function(){
              $("#build-project").click(function(e){
                  e.preventDefault();
                $.ajax({type: "POST",
                        url: "/dashboard/project/build/",
                        data: { project_id: {{ $project->id }} },
                        success:function(result){
                            $.gritter.add({
                              title: 'Build Requested',
                              position: 'bottom-left',
                              text: 'A Build has been scheduled and it will processed as soon as possible',
                              class_name: 'success'
                            });
                            // Refresh the build request
                        },
                        error:function(result){
                              $.gritter.add({
                                title: 'Build Requested',
                                position: 'bottom-left',
                                text: 'Something went wrong',
                                class_name: 'danger'
                              });
                          }
                        });
              });
            });
        }
    </script>


    @if(!empty($scoreData))

    <script type="text/javascript">
    window.addEventListener("load", initializeChart, false);

    function initializeChart(e) {
        if (!jQuery.plot) {
          return;
        }

        var data = [
          @foreach($scoreData->breakdown as $score)
                { label: "{{ Str::title($score->type) }}", data: {{ $score->score }}},
          @endforeach
        ];

        $.plot('#ticket-chart', data, {
          series: {
            pie: {
              show: true,
              innerRadius: 0.45,
              shadow:{
                top: 5,
                left: 15,
                alpha:0.3
              },
              stroke:{
                color:'#333',
                width:0
              },
              label: {
                show: false
              },
              highlight:{
                opacity: 0.08
              }
            }
          },
          grid: {
            hoverable: true,
            clickable: true
          },
          colors: ["#c0392b", "#f1c40f", "#2ecc71"],
          legend: {
            show: false
          }
        });

    }
    </script>

    <script>
    window.addEventListener("load", modal, false);

    function modal(e) {
    $(document).ready(function() {
       $('.md-trigger').modalEffects({
           overlaySelector: '.md-overlay',
           closeSelector: '.md-close',
           classAddAfterOpen: 'md-show',
           modalAttr: 'data-modal',
           perspectiveClass: 'md-perspective',
           perspectiveSetClass: 'md-setperspective',
           afterOpen: function(button, modal) {

           },
           afterClose: function(button, modal) {

           }
       });
      });
    };
    </script>


    <!-- Nifty Modal -->
    <div class="md-modal colored-header warning md-effect-10" id="colored-warning" style="perspective: none;">
        <div class="md-content">
          <div class="modal-header">
            <h3>Custom Header Color</h3>
            <button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <div class="text-center">
              <div class="i-circle warning"><i class="fa fa-warning"></i></div>
              <h4>Run Build!</h4>
              <p>Can you say supercalifragilisticoexpialidoso?</p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-warning btn-flat md-close" data-dismiss="modal">Proceed</button>
          </div>
        </div>
    </div>
    @endif




@else
    <h1>Project Not Found!</h1>
@endif

@stop