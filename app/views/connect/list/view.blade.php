@extends('layouts.landing')
@section('content')

    @if(isset($project->id))
        <div class="row">
            <div class="col-md-4">
                <div class="block-flat dark-box">
                    <div class="header">
                        <div class="actions">
                            <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                            {{--<a class="close-down" href="#"><i class="fa fa-times"></i></a>--}}
                        </div>
                        <h3>{{ $project->name }} <span class="small">by {{ $project->author or 'N/A' }}</span></h3>
                    </div>
                    <div class="content">
                        <p>{{ substr(strip_tags($project->description),0,450)  }}...</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="block-flat visitors">
                    @if(isset($scoreData->avg_score))
                        <h4 class="no-margin">
                            File Score Average : <span class="color-{{ getGradeColor(number_format($scoreData->avg_score,2)) }}">{{ number_format($scoreData->avg_score,2) }}</span>
                            <p>
                                <img src="/badge/grade/{{$project->id}}" />
                                <img src="/badge/score/{{$project->id}}" />
                            </p>
                        </h4>
                        <hr/>
                        <p>Include the badge in your project:</p>
                        <p><strong>Grade: </strong></p>
                        <input type="text" style="width: 100%" value='<img src="http://triplecheck.io/badge/grade/{{$project->id}}" />'/>
                        <p><strong>Score: </strong></p>
                        <input type="text" style="width: 100%" value='<img src="http://triplecheck.io/badge/score/{{$project->id}}" />'/>

                        @if(isset($scoreData->breakdown))
                            <div class="row">
                                <div class="counters col-md-4">
                                    <table class="no-border">
                                        <tbody class="no-border-x">
                                        @foreach($scoreData->breakdown as $score)
                                            @if($score->type != 'healthy')
                                                <tr>
                                                    <td>
                                                        <h4 class="color-{{ $score->type }}">{{ Str::title($score->type) }}</h4>
                                                    </td>
                                                    <td><h4 class="color-{{ $score->type }}">{{ $score->score }}</h4></td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>

                                    </table>

                                </div>
                                <div class="col-md-8">
                                    <div id="ticket-chart" style="height: 140px;"></div>
                                </div>
                            </div>
                        @endif
                    @else
                        <h3>Score Data Not Available</h3>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="block-flat ">
                    @if(isset($scoreData->breakdown))
                        <h4 class="no-margin">File Health Report</h4>
                        <hr/>
                        <table class="no-border">

                            <tbody class="no-border-x">
                            @foreach($scoreData->breakdown as $score)
                                <tr>
                                    <td><h4 class="color-{{ $score->type }}">{{ Str::title($score->type) }}</h4></td>
                                    <td><h4 class="color-{{ $score->type }}">{{ $score->file_count }}</h4></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <h3>Score Data Not Available</h3>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">

        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="block block-color project-table">
                    <div class="header">
                        <div class="actions">
                            <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                            {{--<a class="refresh" href="#"><i class="fa fa-repeat"></i></a>--}}
                        </div>
                        <h3>Issue List</h3>
                    </div>
                    <div class="content">
                        @if(isset($issueList))
                            <table class="no-border">
                                <thead class="no-border">
                                <tr>
                                    <th class="text-left">Plugin</th>
                                    <th class="text-center">Severity</th>
                                    <th style="width:90%;">Issue</th>
                                </tr>
                                </thead>
                                <tbody class="no-border-y">
                                @foreach($issueList as $issue)
                                    <tr>
                                        <td class="text-left">{{ $issue['plugin'] or 'N/A'}}</td>
                                        <td class="text-center"><span
                                                    class="label label-error">{{ $issue['severity'] }}</span></td>
                                        <td><strong>{{ $issue['message'] }}</strong><br><span
                                                    class="color-{{ $issue['type'] }}">{{ Str::title($issue['type']) }}</span>
                                            in Line {{ $issue['line'] }} on file {{ $issue['file'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h4>No Issues Detected</h4>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <div class="block-flat dark-box">
                    <div class="header">
                        <div class="actions">
                            <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                            {{--<a class="close-down" href="#"><i class="fa fa-times"></i></a>--}}
                        </div>
                        <h3>How is the Project Scored?</h3>
                    </div>
                    <div class="content">
                        <p>Currently the scoring is based on the results provided by PHPCodeSniffer and the Magento ECG
                            coding standards, each file is rated using on a scale of 0 to 5, and the overall score is a
                            GPA (Grade point Average) of all the files rated.</p>
                        <h5><strong>File Health Report</strong></h5>

                        <p>Breakdown of the number files analyzed and its overall health.</p>
                        <h5><strong>Issue list</strong></h5>

                        <p>Breakdown of each individual issue found, line number and file where it was found.</p>
                        <h5><strong>File Score Average</strong></h5>

                        <p>Each File is score individually based on the amount of errors and warnings preset.</p>

                        <hr/>
                        <p class="small"><strong>Triplecheck.io</strong> is still on the beta stage. Please don’t
                            hesitate to give us <a href="mailto:amacgregor@demacmedia.com">feedback.</a></p>

                    </div>
                </div>
                <br/>
                @if(!is_null($rewrites))
                    <div class="block-flat">
                        <div class="header">
                            <div class="actions">
                                <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                                {{--<a class="close-down" href="#"><i class="fa fa-times"></i></a>--}}
                            </div>
                            <h3>Rewrite Report</h3>
                        </div>
                        <div class="content">
                            @foreach($rewrites as $filename => $data)
                                <strong>{{ $filename }}</strong>
                                <table class="no-border">
                                    <thead class="no-border">
                                    <tr>
                                        <th class="text-left">Type</th>
                                        <th class="text-left">Class</th>
                                    </tr>
                                    </thead>
                                    <tbody class="no-border-y">
                                    @foreach($data->rewrites as $type => $list)
                                        @foreach($list as $rewrite)
                                            <tr>
                                                <td class="text-left">{{ $type }}</td>

                                                @if(!is_string($rewrite->class) && $type != 'controller')
                                                    <td>---</td>
                                                @elseif($type != 'controller')
                                                    <td class="text-left">{{ $rewrite->class  }}</td>
                                                @else
                                                    <td class="text-left">{{ $rewrite->class->before }}</td>
                                                @endif

                                            </tr>
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <script type="text/javascript">
            window.addEventListener("load", buildRunner, false);
            function buildRunner(e) {
                $(document).ready(function () {
                    $("#build-project").click(function (e) {
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: "/dashboard/project/build/",
                            data: {project_id: {{ $project->id }}},
                            success: function (result) {
                                $.gritter.add({
                                    title: 'Build Requested',
                                    position: 'bottom-left',
                                    text: 'A Build has been scheduled and it will processed as soon as possible',
                                    class_name: 'success'
                                });
                                // Refresh the build request
                            },
                            error: function (result) {
                                $.gritter.add({
                                    title: 'Build Requested',
                                    position: 'bottom-left',
                                    text: 'Something went wrong',
                                    class_name: 'danger'
                                });
                            }
                        });
                    });
                });
            }
        </script>


        @if(isset($scoreData->breakdown))
            <script type="text/javascript">
                window.addEventListener("load", initializeChart, false);

                function initializeChart(e) {
                    if (!jQuery.plot) {
                        return;
                    }

                    var data = [
                        @foreach($scoreData->breakdown as $score)
                        @if($score->type != 'healthy')
                        {label: "{{ Str::title($score->type) }}", data: {{ $score->score }}},
                        @endif
                      @endforeach
                    ];

                    $.plot('#ticket-chart', data, {
                        series: {
                            pie: {
                                show: true,
                                innerRadius: 0.45,
                                shadow: {
                                    top: 5,
                                    left: 15,
                                    alpha: 0.3
                                },
                                stroke: {
                                    color: '#fff',
                                    width: 0
                                },
                                label: {
                                    show: false
                                },
                                highlight: {
                                    opacity: 0.08
                                }
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true
                        },
                        colors: ["#c0392b", "#f1c40f", "#2ecc71"],
                        legend: {
                            show: false
                        }
                    });

                }
            </script>

        @endif
    @else
        <h1>Project Not Found!</h1>
    @endif

@stop