@extends('layouts.landing')
@section('content')
<h1>Magento Connect Report</h1>

<div class="row">
    <div class="col-md-6">
        <div class="block-flat ">
            <h2 class="no-margin">About Triplecheck.io</h2>
            <hr/>
            <p>
            <strong>Triplecheck.io</strong>  is an experimental CI tool built specifically for Magento and currently still in development.
             To showcase and test part of the functionality we downloaded most of the extensions available in
             <a href="http://www.magentocommerce.com/magento-connect/" target="_blank">Magento Connect</a> so that we
             could “score” them.
            </p>

            <p>We built this tool to help our own development teams write better quality code, which as we all know
            reduces probability of bugs and improves maintainability.</p>

            <h2 class="no-margin">The Scoring</h2>
            <hr/>
            <p>The scores are calculated based on the results of PHPCS and the Magento ECG Coding Standards. All the PHP
            and PHTML files inside each extension are analyzed, rated and the overall score is calculated using a GPA (Grade point average) ranging from 0 to 5.
            </p>

        </div>
    </div>
    <div class="col-md-6">
        <div class="block-flat ">
            <h2 class="no-margin">Extensions Analyzed: <strong>{{ $analyzed }}</strong> of {{ $total }}</h2>
            <hr/>
            @foreach($scores as $score)
                <h2 style="padding-top: 5px">
                    <span style="width:{{calculatePercentage($score->count,$total)}}%;float:left;text-align: left;" class="label label-{{getGradeColor($score->score)}}">{{ $score->grade }}</span>
                    <span class="label" style="color: #555">{{ $score->count }} Extensions</span>
                </h2>
            @endforeach
        </div>
    </div>
</div>

<hr/>

@include('includes.connect.project_list')

@stop