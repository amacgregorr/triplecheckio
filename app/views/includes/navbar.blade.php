<nav role="navigation" class="navbar navbar-inverse navbar-flat">
    <div class="container">
        <div class="navbar-header">
          <button data-target="#navbar-collapse-07" data-toggle="collapse" class="navbar-toggle" type="button">
          </button>
          <a href="#" class="navbar-brand">Triplecheck.io</a>
        </div>
        <div id="navbar-collapse-07" class="collapse navbar-right navbar-collapse">
          @if (!Auth::check())
              <p class="navbar-text navbar-right"><a class="navbar-link" href="/login"><i class="fa fa-sign-in"></i> Login</a></p>
          @else
            <p class="navbar-text navbar-right">Signed in as <a class="navbar-link" href="#">{{ Auth::user()->username }}</a> | <a href="/logout"><i class="fa fa-sign-out"></i>Logout</a></p>
          @endif
        </div>
    </div>
</nav>