<div class="row">
    <div class="col-md-6">
        {{$builds->addQuery('order',$order)->addQuery('sort', $sort)->links() }}
    </div>
    <div class="col-md-6  text-right">
        <div class="pagination">
        {{ Form::open(array('action' => 'ConnectController@search')) }}
        {{ Form::label('Extension Name:'); }}
        {{ Form::text('keyword'); }}
        {{ Form::hidden('sort', $sort); }}
        {{ Form::hidden('order', $order); }}
        {{ Form::submit('Search') }}
        {{ Form::close() }}
        </div>
    </div>
</div>

<table id="connect-list" class="tablesorter">
    <thead class="no-border">
        <tr>
            <th class="text-left">Extension Name {{ sortArrow('name',$order) }}</th>
            <th class="text-left">Author {{ sortArrow('author',$order) }}</th>
            <th style="width:60%;">Description</th>
            <th class="text-center">Score {{ sortArrow('score',$order) }}</th>
            <th class="text-center">Details</th>
        </tr>
    </thead>
    <tbody class="no-border-y">
        @foreach($builds as $build)
            <tr>
                <td class="text-left">{{ $build->name }}</td>
                <td class="text-left">{{ $build->author }}</td>
                <td class="text-left">{{ substr(strip_tags($build->description),0,150) }}</td>
                <td data-score="{{ $build->score }}">{{calculateGradeLabel($build->score)}}</td>
                @if($build->build)
                <td>{{link_to_action('ConnectController@view', "Details", $parameters = array("id" => $build->build->project->id ))}}</td>
                @else
                <td></td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>
<div class="row">
    <div class="col-md-6">
        {{$builds->addQuery('order',$order)->addQuery('sort', $sort)->links() }}
    </div>
</div>