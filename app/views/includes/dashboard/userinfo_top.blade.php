<ul class="nav navbar-nav navbar-right user-nav">
  <li class="dropdown profile_menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar" src="http://www.gravatar.com/avatar/{{md5(strtolower(trim(Auth::user()->email))) }}" width="30px" height="30px" /><span>{{Auth::user()->username}}</span> <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="/profile">My Account</a></li>
      <li class="divider"></li>
      <li><a href="/logout">Sign Out</a></li>
    </ul>
  </li>
</ul>