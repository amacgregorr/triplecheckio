<ul class="cl-vnavigation">
    <li><a href="/dashboard"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
    <li><a href="/dashboard/projects"><i class="fa fa-laptop"></i><span>Projects</span></a>
      <ul class="sub-menu">
        @foreach($projects as $project)
        <li><a href="/dashboard/project/{{ $project->id }}">{{ $project->name }}</a></li>
        @endforeach
      </ul>
    </li>
    <li><a href="/dashboard/reports"><i class="fa fa-bar-chart-o"></i><span>Reports</span></a></li>
</ul>