<div class="side-user">
<div class="avatar"><img src="http://www.gravatar.com/avatar/{{md5(strtolower(trim(Auth::user()->email))) }}" width="50px" height="50px" alt="Avatar" /></div>
<div class="info">
  <p>{{ count($projects) }} / 10 <b>Projects</b><span><a href="#"><i class="fa fa-plus"></i></a></span></p>
  <div class="progress progress-user">
    <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%">
      <span class="sr-only">25% Used</span>
    </div>
  </div>
</div>
</div>