<div class="page-head">
 <div class="row" style="margin-top: 0">
    <div class="col-md-9">
        <ol class="breadcrumb">
          <li><a href="/dashboard">Home</a></li>
          @if(isset($breadcrumbs))
              @foreach($breadcrumbs as $key => $value)
                 @if($value != '')
                     <li><a href="{{$value}}">{{$key}}</a></li>
                 @else
                     <li><strong>{{$key}}</strong></li>
                 @endif
              @endforeach
          @endif
        </ol>
    </div>
    @if(isset($currentProject))
    <div class="col-md-3 text-right">
        <div class="btn-group">
              <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown">
                Settings <span class="caret"></span>
              </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/dashboard/project/edit-settings/id/{{ $currentProject->id }}">Edit Project Settings</a></li>
                <li><a href="/dashboard/project/edit-build/id/{{ $currentProject->id }}">Edit Build Settings</a></li>
            </ul>
        </div>
        {{--<button type="button" class="btn btn-success md-trigger" data-modal="colored-warning">Run Build</button>--}}
        <button type="button" id="build-project" class="btn btn-success">Run Build</button>
    </div>
    @endif
 </div>
</div>