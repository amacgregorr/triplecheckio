@extends('layouts.default')
@section('content')
<div class="landing-container">
    <h1> Sh*t happens.  <br/>Knowing about it saves you money.</h1>

    <hr/>

    <div class="text-left">
        <h4>Who are we and why did we build this service?</h4>

        <p>We're the guys and gals behind <a href=""><b>Demac Media</b></a>, one of Magento's top solution partners and winner of the <b>2011 Partner Excellence Award</b>.
            We've been working with Magento since its beginnings and over the years we've encountered the following a little too much:</p>
        <ul>
            <li>Even the best developers are only human and make costly mistakes.</li>
            <li>Store operators have little visibility into what their development teams are doing and if they are (potentially) creating business risk.</li>
            <li>Problems are usually found only after they have caused a business pain (i.e. - lost sales)</li>
            <li>Continuous quality assurance / checking is not common. One time code audits aren't enough when eCommerce stores are constantly changing and adapting.</li>
        </ul>
    </div>
</div>
@stop
