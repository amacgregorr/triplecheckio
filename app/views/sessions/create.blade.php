@extends('layouts.default')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
        <h4>Login</h4>
        <hr/>
        </div>
        <div class="col-md-6 form-container ">
            {{ Form::open(array('route' => 'sessions.store')) }}
            <div class="form-group">
                {{ Form::label('email','Email:') }}
                {{ Form::text('email', '', array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('password','Password:') }}
                {{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required')) }}
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </div><!-- /.col-md-12 -->
    </div>
</div>
@stop