<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{ URL::asset('img/dashboard/icon.png') }}">

	<title>Triplecheck.io - Dashboard</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/vendor/bootstrap.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('js/dashboard/jquery.gritter/css/jquery.gritter.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('js/dashboard/jquery.niftymodals/css/component.css') }}" />
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />

    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{ URL::asset('js/vendor/html5shiv.js') }}"></script>
      <script src="{{ URL::asset('js/vendor/respond.min.js') }}"></script>
    <![endif]-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('js/dashboard/jquery.nanoscroller/nanoscroller.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('js/dashboard/jquery.easypiechart/jquery.easy-pie-chart.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dashboard/style.css') }}"  />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dashboard/skin-green.css') }}"  />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dashboard/custom.css') }}"  />

<style type="text/css" media="screen">
    #task_config {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }
</style>

</head>
<body class="animated">

<div id="cl-wrapper">

  <div class="cl-sidebar">
    <div class="cl-toggle"><i class="fa fa-bars"></i></div>
    <div class="cl-navblock">
      <div class="menu-space">
        <div class="content">
          <div class="sidebar-logo">
            <div class="logo">
                <a href="/dashboard" class="navbar-brand">Triplecheck.io</a>
            </div>
          </div>
            @include('includes.dashboard.userinfo_left')
            @include('includes.dashboard.vertnav')
        </div>
      </div>
      <div class="text-right collapse-button" style="padding:7px 9px;">
        <input type="text" class="form-control search" placeholder="Search..." />
        <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
      </div>
    </div>
  </div>
	<div class="container-fluid" id="pcont">
   <!-- TOP NAVBAR -->
  <div id="head-nav" class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-collapse">
        @include('includes.dashboard.userinfo_top')
      </div><!--/.nav-collapse animate-collapse -->
    </div>
  </div>
    <div class="cl-mcont">
        @include('includes.dashboard.breadcrumb')
        @yield('content')
    </div>
</div>
</div>


<script src="{{ URL::asset('js/dashboard/jquery.js') }}"></script>
<script src="{{ URL::asset('js/dashboard/jquery.cookie/jquery.cookie.js') }}"></script>
<script src="{{ URL::asset('js/dashboard/jquery.pushmenu/js/jPushMenu.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.nanoscroller/jquery.nanoscroller.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.sparkline/jquery.sparkline.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.ui/jquery-ui.js') }}" ></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.gritter/js/jquery.gritter.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.niftymodals/js/jquery.modalEffects.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/behaviour/core.js') }}"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="{{ URL::asset('js/dashboard/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.pie.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.resize.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.labels.js') }}"></script>

<script src="//cdn.jsdelivr.net/ace/1.1.8/min/ace.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>
