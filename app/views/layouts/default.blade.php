<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Triplecheck.io</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ URL::asset('css/vendor/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/flat-ui-pro.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{ URL::asset('js/vendor/html5shiv.js') }}"></script>
      <script src="{{ URL::asset('js/vendor/respond.min.js') }}"></script>
    <![endif]-->
  </head>
  <body>
    @include('includes.navbar')
    <div class="container">
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    @yield('content')
    </div>
    @include('includes.footer')

    <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="{{ URL::asset('js/vendor/jquery.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ URL::asset('js/flat-ui-pro.min.js') }}"></script>

  </body>
</html>
