<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{ URL::asset('img/dashboard/icon.png') }}">

	<title>Triplecheck.io - Dashboard</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet'
		  type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/vendor/bootstrap.min.css') }}"/>
	<link rel="stylesheet" type="text/css"
		  href="{{ URL::asset('js/dashboard/jquery.gritter/css/jquery.gritter.css') }}"/>
	<link rel="stylesheet" type="text/css"
		  href="{{ URL::asset('js/dashboard/jquery.niftymodals/css/component.css') }}"/>
	<link rel="stylesheet" type="text/css"
		  href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>

	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{ URL::asset('js/vendor/html5shiv.js') }}"></script>
	<script src="{{ URL::asset('js/vendor/respond.min.js') }}"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css"
		  href="{{ URL::asset('js/dashboard/jquery.nanoscroller/nanoscroller.css') }}"/>
	<link rel="stylesheet" type="text/css"
		  href="{{ URL::asset('js/dashboard/jquery.easypiechart/jquery.easy-pie-chart.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dashboard/style.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dashboard/skin-green.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dashboard/custom.css') }}"/>
</head>
<body class="animated">


<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<!-- TOP NAVBAR -->
		<div id="head-nav" class="topbar navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<div class="cl-navblock">
						<div class="menu-space">
							<div class="content">
								<div class="sidebar-logo">
									<div class="logo">
										<a href="/" class="navbar-brand">Triplecheck.io</a>
									</div>
								</div>
                                {{--<ul class="nav navbar-nav navbar-right user-nav">--}}
                                    {{--<li class="dropdown profile_menu">--}}
                                        {{--<a style="font-weight: bold; text-transform: uppercase" href="/get-it">Get Your Project on Triplecheck.io</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="cl-mcont">
			@yield('content')
		</div>
	</div>

</div>

<script src="{{ URL::asset('js/dashboard/jquery.js') }}"></script>
<script src="{{ URL::asset('js/dashboard/jquery.cookie/jquery.cookie.js') }}"></script>
<script src="{{ URL::asset('js/dashboard/jquery.pushmenu/js/jPushMenu.js') }}"></script>
<script type="text/javascript"
		src="{{ URL::asset('js/dashboard/jquery.nanoscroller/jquery.nanoscroller.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.sparkline/jquery.sparkline.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.ui/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.gritter/js/jquery.gritter.js') }}"></script>
<script type="text/javascript"
		src="{{ URL::asset('js/dashboard/jquery.niftymodals/js/jquery.modalEffects.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/behaviour/core.js') }}"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="{{ URL::asset('js/dashboard/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.pie.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.resize.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/dashboard/jquery.flot/jquery.flot.labels.js') }}"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-57067424-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
