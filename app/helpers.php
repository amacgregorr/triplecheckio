<?php

function calculateGradeLabel($score)
{
    switch ($score) {
        case ($score >= 5):
            return getGradeLabel('A+','success');
            break;
        case ($score < 5 && $score >=4):
            return getGradeLabel('A','success');
            break;
        case ($score < 4 && $score >=3):
            return getGradeLabel('B','warning');
            break;
        case ($score < 3 && $score >=2):
            return getGradeLabel('C','warning');
            break;
        case ($score < 2 && $score >=1):
            return getGradeLabel('D','danger');
            break;
        case ($score < 1 && $score >=0):
            return getGradeLabel('F','danger');
            break;
        default:
            return getGradeLabel('F','danger');
            break;
    }
}

function calculateGrade($score)
{
    switch ($score) {
        case ($score == 5):
            return 'A+';
            break;
        case ($score < 5 && $score >=4):
            return 'A';
            break;
        case ($score < 4 && $score >=3):
            return 'B';
            break;
        case ($score < 3 && $score >=2):
            return 'C';
            break;
        case ($score < 2 && $score >=1):
            return 'D';
            break;
        case ($score < 1 && $score >=0):
            return 'F';
            break;
        default:
            return 'F';
            break;
    }
}

function getGradeColor($score)
{
    switch ($score) {
        case ($score == 5):
            return 'success';
            break;
        case ($score < 5 && $score >=4):
            return 'success';
            break;
        case ($score < 4 && $score >=3):
            return 'warning';
            break;
        case ($score < 3 && $score >=2):
            return 'warning';
            break;
        case ($score < 2 && $score >=1):
            return 'error';
            break;
        case ($score < 1 && $score >=0):
            return 'error';
            break;
    }
}

function calculatePercentage($score, $total)
{
    $percetange = (($score * 100) / $total);
    return ceil(number_format($percetange, 2));
}

function getGradeLabel($grade, $color)
{
    return "<span class='label label-{$color}'>{$grade}</span>";
}

function sortArrow($sort, $order)
{
    /**
     *                 <a href="{{action('ConnectController@index', array('sort' => 'status', 'order' => 'asc'))}}">
    <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{action('ConnectController@index', array('sort' => 'status', 'order' => 'desc'))}}">
    <i class="fa fa-chevron-down"></i>
    </a>
     */
    $chevron = 'down';
    if($order == 'asc')
    {
        $chevron = 'up';
        $order = 'desc';
    } elseif($order == 'desc') {
        $chevron = 'down';
        $order = 'asc';
    }

    $url = action('ConnectController@index', array('sort' => $sort, 'order' => $order));
    return "<a style='color:#fff' href='{$url}'><i class='fa fa-chevron-{$chevron}'></i></i></a>";
}