# README #

Crimson-CI is a hosted continuous integration service, specially designed to focus on Magento code analysis and quality assurance.


### What is this repository for? ###

#### Quick summary
Crimson-CI has two main functions, run set of Code Analysis tools and generate reports based on the output from those tools. Crimson-Ci will also keep track of the history of all the runs as a way to measure quality overtime.

#### Version

* **MVP:** Basic configuration and reporting, focusing solely on running phpCS and parsing the reports.

As for the structure, we have a project which is tight to a specific repository ( but not a branch) all the configuration is stored into a json string.

```
{
  "configuration": {
    "project": { 
      "name": "Example Project",
      "description": "Example Project Description",
      "url": "http://example/com",
      "authors": [
        { "name": "Allan MacGregor", "email": "amacgregor@demacmedia.com", "github_username": "amacgregor" }  
      ]
    }
  },
  "tasks": [
    { 
      "type": "phpcs",
      "settings": {
        "ignore_list": [
          { "type": "directory", "path": "app/code/core/"}  
        ],
        "file_extensions": ["php", "phtml"],
        "standards": ["ECG","Zend"],
        "report": "json",
        "files": [ "app/code/local/Demac/Bogo/Model/Observer.php", "app/code/local/Demac/"]
      }
    }
  ]
}
```
See: https://gist.github.com/84a676b9b15dae0b49a2.git


##### Important Concepts 

- **Project:** It contains the default configuration name, git/svn repo, descriptions, authors, etc. 
 The project also stores the default configuration for the builds.
 - **Build:** A build is a snapshopt of a project and it belongs to the a specific project, the build will
 copy the project information and the build configuration into a single json object that is unique to that
 build.
 - **Report:** Belong to a build, reports have the specific details from the build (store in json), an average
 score ( pending to define) GBP? CQS ? or just score.

>  These 3 basic models should provide most of the functionality required for a MVP.


#### Builds #####

**Score data**
```
{
  "avg_score": 1.45,
  "breakdown": [
    { "type": "error", "score": 11 },
    { "type": "warning", "score": 8 },
    { "type": "healthy", "score": 2 }
  ]
}
```

**Results**
```
{
 "files": [
    { 
        "path": "app/code/community/Demac/Chase/Model/Paymentech/Method.php",
        "errors": 7,
        "warnings": 1,
        "cqs": 3.2,
        "issues": [
            { "type": "error", "plugin": "phpcs",  "line": 2, "severity": 5, "message": "Missing file doc comment", "file": "/path/to/code/myfile.php" }
         ]
 ]
}
```

#### Runner ####

- Get Builds from the build Queue
- Update / Checkout the repository 
- Load the plugin/task list 
- Run each task
- Process output into build

#### Plugins ####

##### PHPCS #####


```

{
   "totals":{
      "errors":10,
      "warnings":20
   },
   "files":{
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Example.php":{
         "errors":0,
         "warnings":0,
         "messages":[

         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Index.php":{
         "errors":0,
         "warnings":0,
         "messages":[

         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Observer.php":{
         "errors":8,
         "warnings":15,
         "messages":[
            {
               "message":"The method parameter $observer is never used",
               "source":"Generic.CodeAnalysis.UnusedFunctionParameter.Found",
               "severity":5,
               "type":"WARNING",
               "line":55,
               "column":12
            },
            {
               "message":"Model LSD method load() detected in loop",
               "source":"ECG.Performance.Loop.ModelLSD",
               "severity":5,
               "type":"ERROR",
               "line":143,
               "column":20
            },
            {
               "message":"The method parameter $observer is never used",
               "source":"Generic.CodeAnalysis.UnusedFunctionParameter.Found",
               "severity":5,
               "type":"WARNING",
               "line":156,
               "column":12
            },
            {
               "message":"Model LSD method save() detected in loop",
               "source":"ECG.Performance.Loop.ModelLSD",
               "severity":5,
               "type":"ERROR",
               "line":168,
               "column":26
            },
            {
               "message":"Function's cyclomatic complexity (17) exceeds 10; consider refactoring the function",
               "source":"Generic.Metrics.CyclomaticComplexity.TooHigh",
               "severity":5,
               "type":"WARNING",
               "line":178,
               "column":15
            },
            {
               "message":"Avoid function calls in a FOR loop test part",
               "source":"Generic.CodeAnalysis.ForLoopWithTestFunctionCall.NotAllowed",
               "severity":5,
               "type":"WARNING",
               "line":192,
               "column":17
            },
            {
               "message":"Increment operators should be used where possible; found \"$chunkSize += 1;\" but expected \"$chunkSize++\"",
               "source":"Squiz.Operators.IncrementDecrementUsage",
               "severity":5,
               "type":"ERROR",
               "line":216,
               "column":20
            },
            {
               "message":"Avoid function calls in a FOR loop test part",
               "source":"Generic.CodeAnalysis.ForLoopWithTestFunctionCall.NotAllowed",
               "severity":5,
               "type":"WARNING",
               "line":222,
               "column":13
            },
            {
               "message":"Array size calculation function count() detected in loop",
               "source":"ECG.Performance.Loop.ArraySize",
               "severity":5,
               "type":"ERROR",
               "line":235,
               "column":17
            },
            {
               "message":"Function's cyclomatic complexity (11) exceeds 10; consider refactoring the function",
               "source":"Generic.Metrics.CyclomaticComplexity.TooHigh",
               "severity":5,
               "type":"WARNING",
               "line":276,
               "column":15
            },
            {
               "message":"Avoid function calls in a FOR loop test part",
               "source":"Generic.CodeAnalysis.ForLoopWithTestFunctionCall.NotAllowed",
               "severity":5,
               "type":"WARNING",
               "line":285,
               "column":17
            },
            {
               "message":"Increment operators should be used where possible; found \"$chunkSize += 1;\" but expected \"$chunkSize++\"",
               "source":"Squiz.Operators.IncrementDecrementUsage",
               "severity":5,
               "type":"ERROR",
               "line":301,
               "column":20
            },
            {
               "message":"Avoid function calls in a FOR loop test part",
               "source":"Generic.CodeAnalysis.ForLoopWithTestFunctionCall.NotAllowed",
               "severity":5,
               "type":"WARNING",
               "line":309,
               "column":13
            },
            {
               "message":"Array size calculation function count() detected in loop",
               "source":"ECG.Performance.Loop.ArraySize",
               "severity":5,
               "type":"ERROR",
               "line":323,
               "column":16
            },
            {
               "message":"Avoid function calls in a FOR loop test part",
               "source":"Generic.CodeAnalysis.ForLoopWithTestFunctionCall.NotAllowed",
               "severity":5,
               "type":"WARNING",
               "line":362,
               "column":13
            },
            {
               "message":"Model LSD method save() detected in loop",
               "source":"ECG.Performance.Loop.ModelLSD",
               "severity":5,
               "type":"ERROR",
               "line":414,
               "column":33
            },
            {
               "message":"Function's cyclomatic complexity (11) exceeds 10; consider refactoring the function",
               "source":"Generic.Metrics.CyclomaticComplexity.TooHigh",
               "severity":5,
               "type":"WARNING",
               "line":469,
               "column":15
            },
            {
               "message":"Avoid function calls in a FOR loop test part",
               "source":"Generic.CodeAnalysis.ForLoopWithTestFunctionCall.NotAllowed",
               "severity":5,
               "type":"WARNING",
               "line":480,
               "column":17
            },
            {
               "message":"Array size calculation function count() detected in loop",
               "source":"ECG.Performance.Loop.ArraySize",
               "severity":5,
               "type":"ERROR",
               "line":501,
               "column":16
            },
            {
               "message":"The method parameter $item is never used",
               "source":"Generic.CodeAnalysis.UnusedFunctionParameter.Found",
               "severity":5,
               "type":"WARNING",
               "line":565,
               "column":15
            },
            {
               "message":"The method parameter $addToCart is never used",
               "source":"Generic.CodeAnalysis.UnusedFunctionParameter.Found",
               "severity":5,
               "type":"WARNING",
               "line":565,
               "column":15
            },
            {
               "message":"Direct object instantiation (class Mage_Catalog_Model_Product) is discouraged in Magento.",
               "source":"ECG.Classes.ObjectInstantiation.DirectInstantiation",
               "severity":5,
               "type":"WARNING",
               "line":619,
               "column":20
            },
            {
               "message":"Function's cyclomatic complexity (12) exceeds 10; consider refactoring the function",
               "source":"Generic.Metrics.CyclomaticComplexity.TooHigh",
               "severity":5,
               "type":"WARNING",
               "line":666,
               "column":15
            }
         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Mysql4\/Index.php":{
         "errors":0,
         "warnings":1,
         "messages":[
            {
               "message":"Mysql4 classes are obsolete.",
               "source":"ECG.Classes.Mysql4.Found",
               "severity":5,
               "type":"WARNING",
               "line":9,
               "column":7
            }
         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Mysql4\/Index\/Collection.php":{
         "errors":0,
         "warnings":1,
         "messages":[
            {
               "message":"Mysql4 classes are obsolete.",
               "source":"ECG.Classes.Mysql4.Found",
               "severity":5,
               "type":"WARNING",
               "line":9,
               "column":7
            }
         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Resource\/Mysql4\/Setup.php":{
         "errors":0,
         "warnings":1,
         "messages":[
            {
               "message":"Mysql4 classes are obsolete.",
               "source":"ECG.Classes.Mysql4.Found",
               "severity":5,
               "type":"WARNING",
               "line":10,
               "column":7
            }
         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Api.php":{
         "errors":0,
         "warnings":0,
         "messages":[

         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Indexer\/Prices.php":{
         "errors":2,
         "warnings":2,
         "messages":[
            {
               "message":"Function's nesting level (6) exceeds 5; consider refactoring the function",
               "source":"Generic.Metrics.NestingLevel.TooHigh",
               "severity":5,
               "type":"WARNING",
               "line":46,
               "column":12
            },
            {
               "message":"Comment refers to a TODO task \"Fix with better Validation\"",
               "source":"Generic.Commenting.Todo.TaskFound",
               "severity":5,
               "type":"WARNING",
               "line":79,
               "column":139
            },
            {
               "message":"Model LSD method save() detected in loop",
               "source":"ECG.Performance.Loop.ModelLSD",
               "severity":5,
               "type":"ERROR",
               "line":87,
               "column":46
            },
            {
               "message":"Model LSD method delete() detected in loop",
               "source":"ECG.Performance.Loop.ModelLSD",
               "severity":5,
               "type":"ERROR",
               "line":102,
               "column":21
            }
         ]
      },
      "\/media\/projects\/Demac\/appg-csvc01\/trunk\/app\/code\/local\/Demac\/Bogo\/Model\/Api\/V2.php":{
         "errors":0,
         "warnings":0,
         "messages":[

         ]
      }
   }
}
```
### Scoring ###

The Score builder takes each plugin and the result of the plugin run, scoring each individually and then factoring the
results into a general score 

### Model and Entity Responsibilities ###

#### Models:

- ** Project:** Main entity holds information like configuration, vcs settings, description
- ** BuildQueue:** Description
- ** Plugin:** Description

There is a base abstract metric class and a matching interface, that specifies the necessary and common methods for 
running, processing and storing the data for each of tool. 

### Roadmap ###

#### 0.1.0 ####

Initial release version with focus solely on Static Code Analysis

#### 0.2.0 ####

Addition and integration of testing tools like phpunit and phpspec 




### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact